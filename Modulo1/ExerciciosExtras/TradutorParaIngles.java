

public class TradutorParaIngles implements Tradutor{
    
    public String traduzir( String textoEmPortugues ){
        switch(textoEmPortugues){ // Recebe um valor e compara com os casos que ele tem
          case "Sim": 
            return "yes";
          case "Obrigado":
          case "Obrigada":
            return "Thank you";
          default:
            return null;
        }
    }
   
    
    
}
