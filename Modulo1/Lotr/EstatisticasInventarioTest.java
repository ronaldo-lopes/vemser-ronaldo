
import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class EstatisticasInventarioTest{

@Test
public void calcularMediaInventarioVazio(){
  Inventario inventario = new Inventario(1);
  EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
  assertTrue(Double.isNaN(estatisticas.calcularMedia()));
}

@Test
public void calcularMediaApenasUmItem(){
  Inventario inventario = new Inventario(1);
  inventario.adicionar(new Item(2, " Escudo "));
  EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
  assertEquals(2, estatisticas.calcularMedia(),1e-9);
}    

@Test
public void calcularMediaComDoisItens(){
  Inventario inventario = new Inventario(1);
  inventario.adicionar(new Item(2, " Escudo "));
  inventario.adicionar(new Item(4, " Espada "));
  EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
  assertEquals(3, estatisticas.calcularMedia(),1e-9);
}

/*@Test
public void calcularAMediaDosItens(){
  Inventario inventario = new Inventario(4);
  
  EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
  Item espada = new Item(5, "Espada");
  Item escudo = new Item(2, "Escudo");
  Item flecha = new Item(10,"Flecha");
  Item lanca = new Item(5, "lanca");
  inventario.adicionar(espada);
  inventario.adicionar(escudo);
  inventario.adicionar(flecha);
  inventario.adicionar(lanca);
  
  estatistica.calcularMedia();
  //assertEquals(espada, estatistica.calculaMedia());
  //assertEquals(escudo, estatistica.calculaMedia());
  //assertEquals(flecha, estatistica.calculaMedia());
  //assertEquals(lanca, estatistica.calculaMedia());
  //assertEquals(4,estatistica.calculaMedia());
 


}

@Test
public void calcularAMedianaDosItens(){
  Inventario inventario = new Inventario(4);
  
  EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
  Item espada = new Item(5, "Espada");
  Item escudo = new Item(2, "Escudo");
  Item flecha = new Item(10,"Flecha");
  Item lanca = new Item(5, "lanca");
  inventario.adicionar(espada);
  inventario.adicionar(escudo);
  inventario.adicionar(flecha);
  inventario.adicionar(lanca);
  
  estatistica.calculaMediana();    
    
    
}

@Test
public void calcularItensAcimaDaMedia(){
  Inventario inventario = new Inventario(4);
  
  EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
  Item espada = new Item(5, "Espada");
  Item escudo = new Item(2, "Escudo");
  Item flecha = new Item(10,"Flecha");
  Item lanca = new Item(5, "lanca");
  inventario.adicionar(espada);
  inventario.adicionar(escudo);
  inventario.adicionar(flecha);
  inventario.adicionar(lanca);
  
  estatistica.qtdItensAcimaDaMedia();    
  //assertEquals(flecha, estatistica.qtdItensAcimaDaMedia());  
    
}*/
}
