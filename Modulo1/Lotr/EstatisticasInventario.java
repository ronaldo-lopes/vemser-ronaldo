import java.util.*;

public class EstatisticasInventario{
    private Inventario inventario;
    
    public EstatisticasInventario(Inventario inventario){
       this.inventario = inventario;
    }
    
    public Inventario getInventario(){
       return this.inventario;
    
    }
    
    public double calcularMedia(){
        if(this.inventario.getItens().isEmpty()){ // verifica se esta vazio a lista
           return Double.NaN;
        }
        
        double somaQtds = 0;
        for ( Item item : this.inventario.getItens() ){
            somaQtds += item.getQuantidade();
        }
        
        return somaQtds / this.inventario.getItens().size();
        
        
    }
    
    
    public double calculaMediana(){
      if(this.inventario.getItens().isEmpty()){ // verifica se esta vazio a lista
           return Double.NaN;
      }
      
      int qtdItens = this.inventario.getItens().size();
      int meio = qtdItens / 2;
      int qtdMeio = this.inventario.obter(meio).getQuantidade();
      if( qtdItens % 2 == 1 ){
        return qtdMeio;  
        
      }
      
      int qtdMeioMenosUm = this.inventario.obter(meio - 1).getQuantidade();
      return ( qtdMeio + qtdMeioMenosUm ) / 2.0;
      
    }
    
    
    public int qtdItensAcimaDaMedia(){
      int acimaDaMedia = 0;
      double media = this.calcularMedia();//this para chamar o metodo da mesma classe
      
      for ( Item item : this.inventario.getItens() ){
         if( item.getQuantidade() > media ){
           acimaDaMedia++;   
         } 
      }
        
         return acimaDaMedia;
    }
    
}
