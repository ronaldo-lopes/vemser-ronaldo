import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoVerdeTest{

  @Test
   public void elfoVerdeGanha2XPorFlecha(){
     ElfoVerde celebron = new ElfoVerde("Celebron");
     celebron.atirarFlecha(new Dwarf("Balin"));
     assertEquals(2, celebron.getExperiencia());
  }
  
  @Test
   public void elfoVerdeAdicionarItemComDescricaoValida(){
     ElfoVerde celebron = new ElfoVerde("Celebron");
     Item arcoDeVidro = new Item(1, "Arco de vidro");
     
     celebron.ganharItem(arcoDeVidro);
     Inventario inventario = celebron.getInventario();
     assertEquals(new Item(1, "Flecha"), inventario.obter(0));
     assertEquals(new Item(1, "Arco"), inventario.obter(1));
     assertEquals(arcoDeVidro, inventario.obter(2));
  }
  
  
  @Test
   public void elfoVerdeAdicionarItemComDescricaoInvalida(){
     ElfoVerde celebron = new ElfoVerde("Celebron");
     Item arcoDeVidro = new Item(1, "Arco de Madeira");
     celebron.ganharItem(arcoDeVidro);
     Inventario inventario = celebron.getInventario();
     assertEquals(new Item(1, "Flecha"), inventario.obter(0));
     assertEquals(new Item(1, "Arco"), inventario.obter(1));
     assertNull(inventario.buscar("Arco de Madeira"));
  }
  
  @Test
   public void elfoVerdePerdeItemComDescricaoValida(){
     ElfoVerde celebron = new ElfoVerde("Celebron");
     Item arcoDeVidro = new Item(1, "Arco de Madeira");
     celebron.perderItem(arcoDeVidro);
     Inventario inventario = celebron.getInventario();
     assertEquals(new Item(1, "Flecha"), inventario.obter(0));
     assertEquals(new Item(1, "Arco"), inventario.obter(1));
     assertNull(inventario.buscar("Arco de Vidro"));
  }
  
  @Test
   public void elfoVerdePerdeItemComDescricaoInvalida(){
     ElfoVerde celebron = new ElfoVerde("Celebron");
     Item arcoDeVidro = new Item(1, "Arco de Madeira");
     celebron.perderItem(arcoDeVidro);
     Inventario inventario = celebron.getInventario();
     assertEquals(new Item(1, "Flecha"), inventario.obter(0));
     assertEquals(new Item(1, "Arco"), inventario.obter(1));
       }
   
}
