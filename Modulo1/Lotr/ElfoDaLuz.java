import java.util.*;


public class ElfoDaLuz extends Elfo{
    
    private int qtdAtaques;
    private final double QTD_VIDA_GANHA = 10;
    
    {
      qtdAtaques = 0;
      
    
    }
    //Criar ArrayList para evitar duplicaçao de codigo
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
         "Espada de galvorn"
         )
       );
    
    public ElfoDaLuz( String nome ){
     super(nome);
     this.qtdDano = 21.0;
     super.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));   
    }
    
    private boolean devePerderVida(){
      return qtdAtaques % 2 == 1;
    }
    
    public void ganharVida(){
      vida += QTD_VIDA_GANHA;
    } 
    
    @Override
    public void perderItem( Item item ){
      boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains( item.getDescricao());
      if(possoPerder){
        super.perderItem(item);  
      }
    }
    
    public void atacarComEspada( Dwarf dwarf ) {
      if( this.getStatus() != Status.MORTO ){
         dwarf.sofrerDano();
         this.aumentarXp();
         qtdAtaques++;
         if (devePerderVida()){
            this.sofrerDano();
         } else{
            this.ganharVida();
         }
      }
    }
}
