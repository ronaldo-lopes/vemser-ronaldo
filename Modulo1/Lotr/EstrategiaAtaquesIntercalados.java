import java.util.*;

public class EstrategiaAtaquesIntercalados extends ExercitoElfos implements EstrategiaDeAtaque{
    private Sorteador sorteador;
    
    private ArrayList<Elfo> ordenacao(ArrayList<Elfo> elfos){
     Collections.sort(elfos, new Comparator<Elfo>(){
        public int compare( Elfo elfoAtual, Elfo proximoElfo){
          boolean tipoDiferente = elfoAtual.getClass() != proximoElfo.getClass();
          
          if ( tipoDiferente ){
             return 0;
            }
            
             return elfoAtual instanceof ElfoVerde || proximoElfo instanceof ElfoNoturno ? -1 : 1;
        }
        });
        
         return elfos;
    }
    
    public EstrategiaAtaquesIntercalados(){
      sorteador = new DadoD6();
    }
    
    @Override
    public ArrayList<Elfo> buscar( Status status ){
       return this.porStatus.get(status);   
     }
    
    public ArrayList<Elfo> getOrdemAtaque(ArrayList<Elfo> atacantes){ 
          return ordenacao(atacantes);
        
    }
}
