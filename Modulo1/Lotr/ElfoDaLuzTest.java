

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class ElfoDaLuzTest
{
   @Test
   public void elfoDaLuzFazAtaqueParEPerdeVida(){
     ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
     Dwarf farlum = new Dwarf("farlum");
     Inventario inventario = feanor.getInventario();
    
     feanor.atacarComEspada(farlum);
     
     assertEquals(new Item(1, "Flecha"), inventario.obter(0));
     assertEquals(new Item(1, "Arco"), inventario.obter(1));
     assertEquals(new Item(1, "Espada de Galvorn"), inventario.obter(2));
     assertEquals(90.0, feanor.getVida(), 1e-9 );
     assertEquals(1, feanor.getExperiencia());
     ;
  }
  
    @Test
   public void elfoDaLuzFazAtaqueImparEPerdeVida(){
     ElfoDaLuz feanor = new ElfoDaLuz("Feanor");
     Dwarf farlum = new Dwarf("farlum");
     Inventario inventario = feanor.getInventario();
     
     feanor.atacarComEspada(farlum);
     
     assertEquals(new Item(1, "Flecha"), inventario.obter(0));
     assertEquals(new Item(1, "Arco"), inventario.obter(1));
     assertEquals(new Item(1, "Espada de Galvorn"), inventario.obter(2));
     assertEquals(79.0, feanor.getVida(), 1e-9 );
     assertEquals(1, feanor.getExperiencia());
     ;
  }
}
