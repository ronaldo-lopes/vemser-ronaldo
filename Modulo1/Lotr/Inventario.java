import java.util.*;


public class Inventario{
   
   
   private ArrayList<Item> itens;
   private TipoOrdenacao ordenacao;
   
  
   public Inventario(int quantidade){
      this.itens = new ArrayList<Item>(quantidade);
   }
   
   
   public void adicionar( Item item ){
       this.itens.add(item);
      
   }
      
   public Item obter( int posicao ){
      if( posicao >= this.itens.size() ){ 
        return null;
      } 
    return this.itens.get(posicao);
   }
   
   public void remover( Item item ){
      this.itens.remove(item);
   }
   
   public ArrayList<Item> getItens(){
     return this.itens;
   }
   
   // Metodo que lista os nomes dos intens contidos na mochila -> exercicio 2  
   public String getDescricoesItens(){
      StringBuilder descricoes = new StringBuilder();
      for ( int i = 0; i < this.itens.size(); i++ ){
          Item item = this.itens.get(i);
          if ( item != null ){
             descricoes.append(item.getDescricao());
             descricoes.append(",");
          } 
      
      }
      
      //return descricoes.substring(0, (descricoes.length() - 1));
      return (descricoes.length() > 0 ? descricoes.substring(0, (descricoes.length()-1)) : descricoes.toString() );
       
     
   }
   
   
   // Metodo que retorna a posiçao com maior quantidade de itens - > exercicio 3
   public Item getItemComMaiorQuantidade(){
     
     int indice = 0, maiorQuantidade = 0;
     for( int i = 0; i < this.itens.size(); i++ ){
        Item item = this.itens.get(i);
       if(item != null){
          if ( item.getQuantidade() > maiorQuantidade ) {
           maiorQuantidade = item.getQuantidade();
           indice = i;
           
          }
       } 
    }
     return this.itens.size() > 0 ? this.itens.get(indice) : null;
   }  
   
   public Item buscar( String descricao ){
       for ( Item itemAtual : this.itens ){
         boolean encontrei = itemAtual.getDescricao().equals(descricao);
         if( encontrei ){
           return itemAtual;
         }
         
       }
       return null;
    }
   
   public ArrayList<Item> inverter(){
     ArrayList<Item> inverter = new ArrayList<>(this.itens.size());
       for ( int i = this.itens.size() - 1; i >=0; i--){
             inverter.add(this.itens.get(i));
       } 
     return inverter;
   }
   
   //Método que ordena os itens do inventario 
   public void ordenarItens(){
     this.ordenarItens(ordenacao.ASC);
   }
   
   // Método que verifica se a ordenação é crescente ou decrescente
   public void ordenarItens(TipoOrdenacao ordem){
      for(int i = 0; i < this.itens.size(); i++){
        for( int j = 0; j < this.itens.size() - 1; j++){
           Item atual = this.itens.get(j);
           Item proximo = this.itens.get(j+1);
         
           boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ?
           atual.getQuantidade() > proximo.getQuantidade() : 
           atual.getQuantidade() < proximo.getQuantidade();
         
           if( deveTrocar ){
             Item itemTrocado = atual;
             this.itens.set(j, proximo);
             this.itens.set(j + 1, itemTrocado);
           }
         
        }
      }
   }
   
   //Lista de Reforço Exercicio 1
   public ArrayList<Item> unirIventario(ArrayList<Item> novo){
     for(int i = 0; i < novo.size(); i++){
     novo = new ArrayList<Item>(i);
     this.itens = novo;   
     return novo;  
     
     }
     return null; 
   }
   
   //Lista de Reforço Exercicio 2
   public ArrayList<Item> diferenciarIventario(ArrayList<Item> novo){
     for(int i = 0; i < novo.size(); i++){
     novo = new ArrayList<Item>(i);
     if(this.itens.get(i) != novo.get(i)){
     return novo;   
     }
     }
     return null;   
   }
   
   //Lista de Reforço Exercicio 3
   public ArrayList<Item> cruzarIventario(ArrayList<Item> novo){
   for(int i = 0; i < novo.size(); i++){
    novo = new ArrayList<Item>(i);
    if(this.itens.get(i) == novo.get(i)){
       return this.itens = novo;
    }
   }
    return null;
   }
}
