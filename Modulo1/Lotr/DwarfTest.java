import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
   @Test
   public void dwarfNasceCom110DeVida(){
      Dwarf dwarf = new Dwarf("anao");
      //1e-9 -> representaçao simbolica indicando que o 1 fica na 9° casa - 0.000000001
      assertEquals(110.0, dwarf.getVida(),1e-9);
    }
   
   @Test
   public void dwarfPerde10DeVida(){
     Dwarf dwarf = new Dwarf("anao");
     dwarf.sofrerDano();
     assertEquals(100.0, dwarf.getVida(),1e-9);
    }
   
   @Test
   public void dwarfPerdeTodaVida(){
     Dwarf dwarf = new Dwarf("anao");
     
     for (int i = 0; i < 11; i++){
        dwarf.sofrerDano();
     }
     
     assertEquals(0.0, dwarf.getVida(),1e-9);
     
     
    
    }
    
   @Test
   public void dwarfPerdeTodaVida12Ataques(){
     Dwarf dwarf = new Dwarf("anao");
     
     for (int i = 0; i < 12; i++){
        dwarf.sofrerDano();
     }
     
     assertEquals(0.0, dwarf.getVida(),1e-9);
     
    } 
   
 
   @Test
   public void dwarfNasceComStatus(){
     Dwarf dwarf = new Dwarf("anao");
     assertEquals(Status.RECEM_CRIADO, dwarf.getStatus());
   }
   
   @Test
   public void dwarfPerdeTodaVidaEContinuaVivo() {
       Dwarf dwarf = new Dwarf("anao");
       dwarf.sofrerDano();
       assertEquals(Status.SOFREU_DANO, dwarf.getStatus() );
   }
   
   
   @Test
   public void dwarfPerdeTodaVida11AtaquesStatusMorto() {
       Dwarf dwarf = new Dwarf("anao");
       for (int i = 0; i < 11; i++) {
           dwarf.sofrerDano();
        }
       
       assertEquals(Status.MORTO, dwarf.getStatus());
   }
   
   @Test
    public void dwarfPerdeVidaEContinuaVivo(){
        Dwarf dwarf = new Dwarf("anao");
        dwarf.sofrerDano();
        assertEquals( Status.SOFREU_DANO, dwarf.getStatus() );
    }
    
    @Test
    public void dwarfPerdeVidaEDeveMorrer(){
        Dwarf dwarf = new Dwarf("anao");
        
        for( int i = 0; i < 11; i++ ) {
            dwarf.sofrerDano();
        }
        
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
    @Test
    public void dwarfNasceComEscudoNoInventario(){
        Dwarf dwarf = new Dwarf("anao");
        assertEquals("Escudo", dwarf.getInventario().buscar("Escudo").getDescricao());
    }
    
    @Test
    public void dwarfEquipaEscudoETomaMetadeDano(){
        Dwarf dwarf = new Dwarf("anao");
        dwarf.equiparEscudo();
        dwarf.sofrerDano();
        assertEquals(105.0, dwarf.getVida(), 1e-9);
    }
    
    @Test
    public void dwarfNaoEquipaEscudoETomaDanoIntegral(){
        Dwarf dwarf = new Dwarf("anao");
        dwarf.sofrerDano();
        assertEquals(100.0, dwarf.getVida(), 1e-9);
    }
    
   
}