import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest{
    @After
    public void tearDown(){
      System.gc();
    }
    /*
    @Test
    public void atirarFlechaDiminuirFlechaAumentarXp(){
      Elfo novoElfo = new Elfo("Legolas");
      Dwarf novoDwarf = new Dwarf("anao");
     
      novoElfo.atirarFlecha(novoDwarf);
      assertEquals(1, novoElfo.getExperiencia());
      assertEquals(1, novoElfo.getQtdFlecha());
     
    }
    
    //Testar atirar flechas 2 vezes
    @Test
    public void atirar2FlechasZerarFlechas2xAumentarXp(){
      Elfo novoElfo = new Elfo("Legolas");
      Dwarf novoDwarf = new Dwarf("anao");
      
      novoElfo.atirarFlecha(novoDwarf);
      novoElfo.atirarFlecha(novoDwarf);
      assertEquals(2, novoElfo.getExperiencia());
      assertEquals(0, novoElfo.getQtdFlecha());
      
    }
    
    //Testar atirar flechas 3 vezes
    @Test
    public void atirar3FlechasZerarFlechas2xAumentarXp(){
      Elfo novoElfo = new Elfo("Legolas");
      Dwarf novoDwarf = new Dwarf("anao");
      
      novoElfo.atirarFlecha(novoDwarf);
      novoElfo.atirarFlecha(novoDwarf);
      novoElfo.atirarFlecha(novoDwarf);
      assertEquals(3, novoElfo.getExperiencia());
      assertEquals(0, novoElfo.getQtdFlecha());
      
    }
    */
    
    @Test
    public void elfoNasceComStatusRecemCriado(){
      Elfo novoElfo = new Elfo("Legolas");
      assertEquals(Status.RECEM_CRIADO, novoElfo.getStatus());
    }
    
    @Test
    public void naoCriaElfoNaoIncrementa(){
      assertEquals(0, Elfo.getQtdElfos());
    }
    
    @Test
    public void cria1ElfoContadorUmaVez(){
        Elfo novoElfo = new Elfo("Legolas"); 
        assertEquals(1, Elfo.getQtdElfos());
    }
    
     @Test
    public void cria2ElfoContadorDuasVez(){
        Elfo novoElfo = new Elfo("Legolas");
        Elfo novoElfo2 = new Elfo("Legolas");
        assertEquals(2, Elfo.getQtdElfos());
    }
    
    
}
