import java.util.*;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class InventarioTest{
  /*@Test
  public void criarInventarioQuantidadeInformada(){
    Inventario inventario = new Inventario();
    //assertEquals(99, inventario.getItens().length);
    assertEquals(inventario.getItens().size());
  }
  
  @Test
  public void criarInventarioComQuantidadeInformada(){
    Inventario inventario = new Inventario(42);
    //assertEquals(42, inventario.getItens().length);
    assertEquals(inventario.getItens().size());
  }
  */
  @Test
  public void adicionarUmItem(){
    Inventario itens = new Inventario(10);
    Item espada = new Item(1, "Espada");
    itens.adicionar(espada);
    assertEquals(espada, itens.obter(0));// poderia ser tambem asserEquals(espada, inventario.obter[0])
  }
  
  
  @Test
  public void adicionarDoisItens(){
    Inventario itens = new Inventario(10);
    Item espada = new Item(1, "Espada");
    Item escudo = new Item(1, "Escudo");
    itens.adicionar(espada);
    itens.adicionar(escudo);
    assertEquals(espada, itens.obter(0));// poderia ser tambem asserEquals(espada, inventario.obter[0])
    assertEquals(escudo, itens.obter(1));// poderia ser tambem asserEquals(escudo, inventario.obter[0])
  }
  
  @Test
  public void obterItem(){
    Inventario itens = new Inventario(10);
    Item espada = new Item(1, "Espada");
    itens.adicionar(espada);
    assertEquals(espada, itens.obter(0));// poderia ser tambem asserEquals(espada, inventario.obter(0))
    
  }
  
 
  
  @Test
  public void removerItem(){
    Inventario itens = new Inventario(10);
    Item espada = new Item(1, "Espada");
    Item escudo = new Item(1, "Escudo");
    itens.adicionar(espada);
    itens.adicionar(escudo);
    itens.remover(escudo);
    itens.getItens();
    
  }
  
  
  @Test
  public void getDescricoesVariosItens(){
     Inventario itens = new Inventario(10);
     Item espada = new Item(1, "Espada");
     Item escudo = new Item(1, "Escudo");
     Item lanca = new Item(1, "Lança");
     itens.adicionar(espada);
     itens.adicionar(escudo);
     itens.adicionar(lanca);
     assertEquals("Espada,Escudo,Lança", itens.getDescricoesItens());
  }
  
  @Test
  public void getItemMaiorQuantidade(){
     Inventario itens = new Inventario(10);
     Item espada = new Item(1, "Espada");
     Item escudo = new Item(5, "Escudo");
     Item lanca = new Item(3, "Lança");
     itens.adicionar(espada);
     itens.adicionar(escudo);
     itens.adicionar(lanca);
     assertEquals(escudo, itens.getItemComMaiorQuantidade());
  }
 
  @Test
  public void buscarItemDescricao(){
   Inventario itens = new Inventario(10);
   Item espada = new Item(1, "Espada");
   itens.adicionar(espada);
   assertEquals(espada, itens.buscar("Espada"));
   
  }
  
    
  @Test
  public void inverterInventarioVazio(){
   Inventario itens = new Inventario(10);
   assertTrue(itens.inverter().isEmpty());
   
   
    
  }
  
  @Test
  public void inverterInventarioUmItem(){
   Inventario itens = new Inventario(10);
   Item espada = new Item(1, "Espada");
   itens.adicionar(espada);
   assertEquals(espada, itens.inverter().get(0));
   assertEquals(1, itens.inverter().size());
   
   
    
  }
  
  @Test
  public void inverterInventarioDoisItens(){
   Inventario itens = new Inventario(10);
   Item espada = new Item(1, "Espada");
   Item escudo = new Item(1, "Escudo");
   itens.adicionar(espada);
   itens.adicionar(escudo);
      
   assertEquals(escudo, itens.inverter().get(0));
   assertEquals(espada, itens.inverter().get(1));
   assertEquals(2, itens.inverter().size());
   
    
  }
  
 
  /*@Test
  public void mostrarItens(){
    Inventario inventario = new Inventario();
    Item espada = new Item(1, "Espada");
    Item escudo = new Item(1, "Escudo");
    inventario.adicionar(espada);
    inventario.adicionar(escudo);
    inventario.nomeDosItens();
    
    
  }*/
  
  @Test
  public void ordenarItensNoInventario(){
   Inventario itens = new Inventario(10);
   Item espada = new Item(1, "Espada");
   Item escudo = new Item(1, "Escudo");
   itens.adicionar(espada);
   itens.adicionar(escudo);
      
   itens.ordenarItens();
   
    
  }
  
  @Test
  public void ordenarAscendenteDosItensNoInventario(){
   Inventario itens = new Inventario(10);
   Item espada = new Item(1, "Espada");
   Item escudo = new Item(1, "Escudo");
   Item lanca = new Item(1, " Lanca ");
   itens.adicionar(espada);
   itens.adicionar(escudo);
   itens.adicionar(lanca);
      
   itens.ordenarItens(TipoOrdenacao.ASC);
   
    
  }
  
  @Test
  public void ordenarDescendenteDosItensNoInventario(){
   Inventario itens = new Inventario(10);
   Item espada = new Item(1, "Espada");
   Item escudo = new Item(1, "Escudo");
   Item lanca = new Item(1, " Lanca ");
   itens.adicionar(espada);
   itens.adicionar(escudo);
   itens.adicionar(lanca);
      
   itens.ordenarItens(TipoOrdenacao.DESC);
   
    
  }
  
    
}
