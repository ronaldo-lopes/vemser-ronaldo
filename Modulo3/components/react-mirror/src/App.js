
import React, { Component} from 'react';
import './App.css';

//import CompA, { CompB } from './components/exemploComponenteBasico';
import Membros from './components/Membros';

class App extends Component { // eslint-disable-next-line
  constructor( props ) {
    super( props );
  }


  render(){
    return (
      <div className="App">
        <Membros nome="João" sobrenome="silva"/>
        <Membros nome="Maria" sobrenome="silva"/>
        {
        /*
        <CompA />
        <CompA />
        <CompB />
        */
      }
       
      </div>
    );
  }
}
export default App;
