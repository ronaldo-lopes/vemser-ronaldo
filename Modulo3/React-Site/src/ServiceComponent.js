import React, { Component } from 'react';
import Work from "./Components/Work-component/Work";
import Button from "./Components/Button-component/Button";
import Header from './Components/Header-component/Header';
import Footer from './Components/Footer-component/Footer';
import Artigo from './Components/ArticleComponent/Article';

import './Components/Work-component/work.css'
import './CSS/grid.css';
import './CSS/reset.css';

export default class ServiceComponent extends Component { // eslint-disable-next-line
  render(){
    return (
     <React.Fragment>
         <Header/>
         <section className="container work">
             <div className="row">
                 <Work/>
             </div>
             <div className="row">
                 <Work/>
             </div>
            <div className="row">
            <article class="col col-12">
                <Artigo titulo={<h3>Título</h3>}/>
                <Button class={"button button-big button-blue"} href={"#"} text={'Saiba mais'}/>
                </article>
            </div>
         </section>
         <Footer/>
     </React.Fragment>
    );
  }
}