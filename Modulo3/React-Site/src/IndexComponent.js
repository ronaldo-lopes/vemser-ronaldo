import React, { Component } from 'react';
import Header from './Components/Header-component/Header';
import Banner from './Components/Banner-compoent/Banner';
import Box from "./Components/Box-component/Box";
import Footer from './Components/Footer-component/Footer';
import Artigo from './Components/ArticleComponent/Article';
import "./Components/Box-component/Box.css";
import "./CSS/grid.css";
import "./CSS/reset.css";
import img1 from "./Components/Box-component/Linhas-de-Serviço-Agil.png";
import img2 from "./Components/Box-component/Linhas-de-Serviço-DBC_Smartsourcing.png";
import img3 from "./Components/Box-component/Linhas-de-Serviço-DBC_Software-Builder.png";
import img4 from "./Components/Box-component/Linhas-de-Serviço-DBC_Sustain.png";

export default class IndexComponent extends Component { // eslint-disable-next-line
  render(){
    return (
     <React.Fragment>
       <Header/>
       <Banner/>
       <section className="container">
            <div className="row">
                <div className="col col-12 col-md-6 col-lg-6">
                    <Artigo titulo={<h2>Titulo</h2>}/>
                </div>

                <div className="col col-12 col-md-6 col-lg-6">
                    <Artigo titulo={<h2>Titulo</h2>}/>
                </div>
            </div>
            <div className="row">
                <Box class={'col col-12 col-md-6 col-lg-3'} imagem={img1}/> 
                <Box class={'col col-12 col-md-6 col-lg-3'} imagem={img2}/> 
                <Box class={'col col-12 col-md-6 col-lg-3'} imagem={img3}/> 
                <Box class={'col col-12 col-md-6 col-lg-3'} imagem={img4}/> 
            </div>
           </section>
           <Footer/>

     </React.Fragment>
    );
  }
}

