import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import IndexComponent from "./IndexComponent";
import ServiceComponent from "./ServiceComponent";
import AboutUs from "./AboutUsComponent";
import ContactComponent from "./ContactComponent";

import "./App.css";
import "./CSS/grid.css";
import "./CSS/reset.css";

 class App extends Component { // eslint-disable-next-line

  render(){
    return (
      <Router>
        <Route path="/" exact component={ IndexComponent }/>
        <Route path="/Sobre-nos" component={ AboutUs }/>
        <Route path="/Servicos" component={ ServiceComponent }/>
        <Route path="/Contato" component={ ContactComponent }/>
      </Router>
    );
  }
}
export default App;
