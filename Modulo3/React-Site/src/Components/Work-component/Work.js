import React from "react";
import Box from '../Box-component/Box';
import imgService from './Linhas-de-Serviço-Agil.png';
import './work.css';
import '../../CSS/grid.css';
import '../../CSS/reset.css';

export default (props) =>
    <div class={props.class}>
        <Box class={'col col-12 col-md-6 col-lg-4'} imagem={imgService}/>
        <Box class={'col col-12 col-md-6 col-lg-4'} imagem={imgService}/>
        <Box class={'col col-12 col-md-6 col-lg-4'} imagem={imgService}/>
    </div>