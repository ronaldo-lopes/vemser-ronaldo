import React from "react";
import "./Buttons.css"

export default ( props ) => 
        <React.Fragment>
            <a className= { props.class } href={ props.href }>{props.text}</a>
        </React.Fragment>