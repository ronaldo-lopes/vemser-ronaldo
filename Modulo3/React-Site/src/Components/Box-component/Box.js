import React from "react";
import Artigo from '../ArticleComponent/Article';
import Button from '../Button-component/Button';
import './Box.css';

export default (props) =>
    <div class={props.class}>
        <article className="box">
        <div>
            <img src={props.imagem} alt={props.alt}/>
        </div>
        <Artigo titulo={<h4>Título</h4>}/>
        <Button class={ 'button button-green' } href={'#'} text={'Saiba mais'}/>
            </article>
    </div>