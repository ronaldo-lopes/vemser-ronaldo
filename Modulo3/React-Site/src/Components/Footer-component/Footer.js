import React, {Component} from "react";
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import "./Footer.css"

export default class Footer extends Component {

  render() {
    return (
      <footer className="main-footer">
          <nav className="container">
            <ul className="">
             <li>
                <Link to="/">Home</Link>
             </li>
             <li>
                <Link to="/AboutUs">Sobre nós</Link>
             </li>
             <li>
                <Link to="/ServiceComponent">Serviços</Link>
             </li>
             <li>
                <Link to="/ContactComponent">Contato</Link>
             </li>
            </ul>
         </nav>
         <p>
             &copy;Copyright DBC Company - 2019
         </p>
      </footer>
    );
  }
}