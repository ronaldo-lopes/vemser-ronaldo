import React from "react";
import Artigo from '../ArticleComponent/Article';
import './about.css';

export default (props) =>
<div class={props.class}>
    <article class="about">
    <div>
        <img src={ props.imagem } alt={props.alt} />
    </div>
    <Artigo titulo={<h2>Título</h2>}/>
    </article>
</div>