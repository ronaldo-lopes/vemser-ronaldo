import React from "react";
import Artigo from '../ArticleComponent/Article';
import Campo from '../Input-component/Campo';
import './contact.css';
import Button from "../Button-component/Button"

export default (props) =>
<div class={props.class}>
    <article class="contact">
    <Artigo titulo={<h2>Título</h2>}/>
    </article>
    <form class="clearfix" >
        <Campo id={"Nome"}  class={"field"}  type={"text"} placeholder={"Nome"}/>
        <div id="msg-erro-Nome"></div>
        <Campo id={"email"} class={"field"} type={"text"} placeholder={"E-mail"}/>
        <div id="msg-erro-email"></div>
        <Campo id={"telefone"} class={"field"} type={"text"} placeholder={"Telefone"}/>
        <div id="msg-erro-telefone"></div>
        <Campo id={"assunto"} class={"field"}  type={"text"} placeholder={"Assunto"}/>
        <div id="msg-erro-assunto"></div>
        <textarea id="msg" class="field"  placeholder="Mensagem"></textarea>
        <div id="msg-erro-msg"></div>
        <Button id={"btn-check"} class={"button button-green button-right"} type={"submit"} onclick={"validarCampos()"} text={"Enviar"}/>
    </form>
</div>