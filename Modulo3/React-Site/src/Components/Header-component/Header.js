import React, {Component} from "react";
import { BrowserRouter as Router, Route, Link} from 'react-router-dom';
import logo from "./logo-dbc-topo.png"
import "./header.css"

export default class Header extends Component {
    render() {
        return (
            <header className="main-header">
                <nav className="container clearfix">
                    <a className="logo" href="index.html" title="Voltar à home">
                        <img src={logo} alt="DBC Logo"/>
                    </a>

                    <label className="mobile-menu" for="mobile-menu">
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                    <input id="mobile-menu" type="checkbox"/>

                    <ul className="clearfix">
                        <li>
                            <Link to="/">Home</Link>
                        </li>
                        <li>
                            <Link to="/Sobre-Nos">Sobre Nós</Link>
                        </li>
                        <li>
                            <Link to="/Servicos">Serviços</Link>
                        </li>
                        <li>
                            <Link to="/Contato">Contato</Link>
                        </li>
                    </ul>
                </nav>
            </header>
        )
    }
}
    