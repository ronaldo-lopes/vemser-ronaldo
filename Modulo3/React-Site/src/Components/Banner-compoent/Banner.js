import React, {Component} from "react";
import Button from "../Button-component/Button";
import Artigo from "../ArticleComponent/Article";

import "./banner.css"

export default class Banner extends Component {// eslint-disable-next-line
    render() { 
    return (
        <section class="main-banner">
            <article>
            <Artigo titulo={<h1>Titulo</h1>}/>
            <Button class={'button button-outline'} href={"#"} text={'Saiba mais'} />
            </article>
        </section>
        )
    }
}