import React, {Component} from 'react';
import Contact from "./Components/Contact-component/Contact";
import Header from './Components/Header-component/Header';
import Footer from './Components/Footer-component/Footer';
import './Components/Contact-component/contact.css';
import './CSS/grid.css';
import './CSS/reset.css';
import './Components/Work-component/work.css';


export default class ContactComponent extends Component{// eslint-disable-next-line
    render(){
        return(
            <React.Fragment>
                <Header/>
               <section class="container work">
                <div class="row">
                    <Contact class={"col col-12 col-md-6"}/>
                </div>
                <div className="map-container"> 
                    <iframe className="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3454.7200844760855!2d-51.17087028474702!3d-30.01619288189262!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x951977775fc4c071%3A0x6de693cbd6b0b5e5!2sDBC%20Company!5e0!3m2!1spt-BR!2sbr!4v1576870098547!5m2!1spt-BR!2sbr" allowfullscreen=""></iframe> 
                </div>
            </section> 
            <Footer/>
            </React.Fragment>
        )
    }
}