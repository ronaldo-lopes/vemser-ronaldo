import React, {Component} from 'react';
import AboutUs from './Components/About-component/AboutUs';
import Header from './Components/Header-component/Header';
import Footer from './Components/Footer-component/Footer';
import './Components/About-component/about.css';
import './CSS/grid.css';
import './CSS/reset.css';
import Img5 from './Components/About-component/html_css_logo.a34e141d.png';
import Img6 from './Components/About-component/javascript_logo.9d07961b.png';
import Img7 from './Components/About-component/Linhas-de-Serviço-DBC_Smartsourcing.png';
import Img8 from './Components/About-component/spring_logo.f20d312e.png';


export default class AboutUsComponent extends Component{// eslint-disable-next-line
    render(){
        return(
            <React.Fragment>
                <Header/>
                <section className="container about-us">
                    <div class="row">
                        <AboutUs class={'col col-12'} imagem={Img5}/>
                    </div>
                    <div class="row">
                        <AboutUs class={'col col-12'} imagem={Img6}/>
                    </div>
                    <div class="row">
                        <AboutUs class={'col col-12'} imagem={Img7}/>
                    </div>
                    <div class="row">
                        <AboutUs class={'col col-12'} imagem={Img8}/>
                    </div>
                </section>
                <Footer/>
            </React.Fragment>
        )
    }
}