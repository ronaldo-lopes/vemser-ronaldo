import React, { Component } from 'react';
import {  Link } from 'react-router-dom';
import { logout } from '../Api/Auth';
import { history } from '../Api/History';
import Img1 from '../Bank.jpeg';
import '../Style/Header.css';
import '../Style/reset.css';
import '../Style/grid.css';

export default class Header extends Component {
    
    fazerLogout(){
        logout()
        history.push('/')
    }

    render() {
        return (
            <React.Fragment>
            <header className="main-header">
            <nav className="container clearfix">
                <div className="logo">
                <img className="ImgBanco" src={Img1} alt="Banco Digital"/>
                </div>
                <label className="mobile-menu" htmlFor="mobile-menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </label>
                <input id="mobile-menu" type="checkbox"/>
                <ul className="clearfix">
                <li>
                <Link className="link-menu" to="./home"><button className="button button-color"  texto="Home">Home</button></Link>
                </li>
                <li>
                <Link className="link-menu" to="./Agencias"><button className="button button-color"  texto="Agencias">Agencias</button></Link>    
                </li>
                <li>
                <Link className="link-menu" to="./Clientes"><button className="button button-color"  texto="Clientes">Clientes</button></Link>    
                </li>
                <li>
                <Link className="link-menu" to="./TiposContas"><button className="button button-color"  texto="Conta">Tipo de Contas</button></Link>    
                </li>
                <li>
                <Link className="link-menu" to="./ContasClientes"><button className="button button-color"  texto="Conta de clientes">Conta de Clientes</button></Link>      
                </li>
                <button className="button-red" onClick={this.fazerLogout.bind(this)}>Logout</button>
                </ul>
            </nav>
        </header>
        </React.Fragment>

            
        
        )
    }
}