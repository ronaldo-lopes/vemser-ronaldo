import React, { Component } from 'react';
import * as axios from 'axios';
import Header from '../Component/Header';

export default class Clientes extends Component {
    constructor( props ){
        super(props)
        this.state = {
            clientes: []
        }
    }

    componentWillMount(){
        axios.get('http://localhost:1337/clientes', {
            headers:{
                'Authorization' :'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({clientes: res.data.clientes})
        })
    }

    render(){
        const clientes = this.state.clientes

        return(
            <Header>
                {clientes.map(cliente => 
                    <div key={cliente.id}>
                        <h3>Nome do cliente: {cliente.nome}</h3>
                        <p>Cpf: {cliente.cpf} </p>
                        <h4>Agencia do cliente</h4>
                    </div>
                    )}
            </Header>
        )
    }
}