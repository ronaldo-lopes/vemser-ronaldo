import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import '../Style/Box.css';
import '../Style/Login.css';
import Img1 from '../Bank.jpeg';
    
   export default class Login extends Component {
    constructor( props ){
        super(props)
        this.logar = this.logar.bind(this)
        this.state = {
            username: '',
            password: '',
                   
        }
    }
logar

    logar = ( e ) => {
        e.preventDefault();
        const { username, password } = this.state
        if( username && password){ 
            axios.post('http://localhost:1337/login', { 
                email: username,
                password: password
            }).then(( res ) => {
                localStorage.setItem('Authorization', res.data.token);
                localStorage.setItem('Username', username);
                this.props.history.push('/home')
                console.log(res.data);
            }).catch((err) => {
                console.log(err);
                alert("Dados Inválidos!")
            })
            
        }
            
        }

    atualizaCampoEmail( evt ){
        this.setState({ email: evt.target.value });
        console.log(evt.target.value)
    }

    atualizaCampoSenha( evt ){
        this.setState({ password: evt.target.value });
        console.log(evt.target.value)
    }

   

        render() {
            return (
                <React.Fragment>     
                    <div className="login">
                    <img className="ImgBanco" src={Img1} alt="Banco Digital"/>  
                              <div className="box">
                              <h1>Banco Digital</h1>
                              <input type="text" name="email" placeholder="Insira seu Email" onChange={this.atualizaCampoEmail.bind(this)}/>    
                              <input type="password" name="password" placeholder="Insira sua Senha" onChange={this.atualizaCampoSenha.bind(this)}/>
                            </div>   
                            <div className="button">
                             <Link to={{ pathname:'/home'}}>
                             <button className="button button-grey" onClick={this.logar} texto="Logar">Logar</button>
                             </Link>   
                            </div>

                    </div>          
                </React.Fragment>
                        

        );
    }
}

