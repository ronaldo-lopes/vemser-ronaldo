import React, { Component } from 'react';
import Header from '../Component/Header';
import Button from '../Component/Button';
import {Link} from 'react-router-dom';
import '../Style/grid.css';
import '../Style/home.css';

export default class Home extends Component {
    render() {
        return (
            <React.Fragment>
            <Header/>
            <div className="row col-12 col-md-3 lista">
            <ul>
            <li>

            <Link to={{ pathname: "/agencias" }} className="link">
                <Button
                classeNome="button button-gray"
                texto="Agencias"
                />
            </Link>
            </li>

            <li>

           <Link to={{ pathname: "/clientes" }} className="link">
                <Button
                classeNome="button button-gray"
                texto="Clientes"
                />
           </Link>
            
            </li>

            <li>

           <Link to={{ pathname: "/tipoContas" }} className="link">
                <Button
                classeNome="button button-gray"
                texto="Tipos Contas"
                />
           </Link>
            
            </li>

            <li>

          <Link to={{ pathname: "/conta/clientes"}} className="link">
              <Button
                classeNome="button button-gray"
                texto="Conta/clientes"
                />
          </Link>
            </li>
        </ul>
        </div>
        </React.Fragment>
        )
    }
}