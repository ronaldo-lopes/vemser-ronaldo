import React, {Component} from 'react'
import  axios from 'axios';
import ApiBanco from '../Api/ApiBanco';
import Agencia from '../Models/AgenciasUi';
import Header from '../Component/Header';
import { Link } from 'react-router-dom';
import '../Style/Agencias.css';

export default class Agencias extends Component{
    constructor(props){
        super(props)
        this.state = {
            agencias: [],
            search: " "
        }
    }

    componentWillMount(){
        axios.get('http://localhost:1337/agencias', {
            headers: {
                'Authorization': 'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({ agencias: res.data.agencias})
        }).catch( (err) => {
            console.log(err);
        })
    }

    render(){
        const { agencias } = this.state
        
        return(
            <React.Fragment>
            <Header/>
            {agencias.map(agencia => 
            <div key={agencia.id} className="box">
                <ul>
                    { agencias.map((e,i) => <Link to={{pathname:`/agencia/${e.id - 1}`}} key={i} >{e.nome}</Link> )}
                </ul>
            </div>
            
                )}
            </React.Fragment>
            
        );
    }
}