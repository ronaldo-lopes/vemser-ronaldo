import React, { Component } from 'react';
import * as axios from 'axios';
import { Link } from 'react-router-dom';
import Header from '../Component/Header';
import '../Style/grid.css';
import '../Style/TiposConta.css';

export default class TiposContas extends Component{
    constructor(props){
        super(props)
        this.state = {
            tiposConta: []
        }
    }

    componentWillMount(){
        axios.get('http://localhost:1337/tipoContas', {
            headers: {
                'Authorization': 'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                tiposConta: res.data.tipos})
                
            }).catch(( err ) => { 
                console.log(err);
        })
    }

    render(){
        const { tiposConta } = this.state
        return (
            <React.Fragment>
                { tiposConta.map( conta => 
                        <Link to={{ pathname: `/tipoContas/${conta.id}`, state: { tipoConta: conta}}} key={conta.id}>
                        <Header/>
                
                        <div className= "tipo col-12">
                        <h2 key={conta.id}>
                            {conta.nome}
                        </h2>
                        </div>    
                        </Link>
                    )}
            </React.Fragment>
        )
    }
}