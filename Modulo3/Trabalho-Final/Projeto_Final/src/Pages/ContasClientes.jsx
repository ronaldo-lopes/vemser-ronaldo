import React, { Component } from 'react';
import * as axios from 'axios';
import { Link } from 'react-router-dom';

export default class ContasClientes extends Component {
    constructor( props ){
        super(props)
        this.state = {
            ContasClientes: [],
            search: " "
        }
    }

    componentWillMount(){
        axios.get('http://localhost:1337/conta/clientes', {
            headers:{
                'Authorization' : 'banco-vemser-api-fake'
            }
        }).then(res => {
            this.setState({
                ContasClientes: res.data.cliente_x_conta
            })
        })
    }

    render() {
        const { ContasClientes } = this.state
        return(
            <div>
                { ContasClientes.map( contas => 
                    <Link to={{pathname: `/conta/cliente/${contas.id}`, state:{ contas: contas }}} key={contas.id}>
                        <p>Cliente: </p>
                        <h4>Nome: { contas.cliente.nome }</h4>
                        <h4>CPF: { contas.cliente.cpf }</h4>
                        <h2>Conta: { contas.tipo.nome }</h2>
                    </Link>
                    )}
            </div>
        )
    }
}