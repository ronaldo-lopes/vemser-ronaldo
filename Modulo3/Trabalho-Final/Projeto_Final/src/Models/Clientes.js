import React, { Component } from 'react';

export default class Cliente extends Component {
    render(){
        const { cliente } = this.props.location.state
        return (
            <React.Fragment key={cliente.id} >
                <h3>Nome do Cliente: {cliente.nome}</h3>
                <h3>CPF: {cliente.cpf}</h3>
                <h3>Agencia:</h3>
                <h3>Codigo: {cliente.agencia.nome}</h3>
                <h3>Endereço: </h3>
                <h3>Logradouro: {cliente.agencia.endereco.logradouro}</h3>
                <p>Numero: {cliente.agencia.endereco.numero}</p>
                <p>Bairro: {cliente.agencia.endereco.bairro}</p>
                <p>Cidade: {cliente.agencia.endereco.cidade}</p>
                <p>UF: {cliente.agencia.endereco.uf}</p>
            </React.Fragment>
        )
    }
}