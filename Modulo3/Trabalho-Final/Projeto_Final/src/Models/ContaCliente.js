import React, { Component } from 'react';
export default class ContaCliente extends Component {
    render(){
        const { contas } = this.props.location.state 
        return(
            <React.Fragment key={contas.id}>
                <p>Cliente:</p>
                <h4>Nome: {contas.cliente.nome}</h4>
                <h4>CPF: {contas.cliente.cpf}</h4>
                <h2>Conta: {contas.tipo.nome}</h2>
            </React.Fragment>
        )
    }
}