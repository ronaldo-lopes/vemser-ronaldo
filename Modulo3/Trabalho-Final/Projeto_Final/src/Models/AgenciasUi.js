import React, {Component} from 'react';


export default class AgenciasUi extends Component{
    constructor(props){
        super(props)
    }

    render(){
        const { agencias, search} = this.props;
            let agencia = agencias.filter(agencia => agencia.nome.includes(search))

            return (
                agencia.map((agencia) => 
                    <React.Fragment>
                        <div>
                            <h2>Agencia:{agencia.codigo}</h2>
                            <h3>Nome da Agencia: {agencia.nome}</h3>
                            <h4>Endereço: </h4>
                            <p>Logradouro: {agencia.endereco.logradouro}</p>
                            <p>Numero: {agencia.endereco.numero} </p>
                            <p>Bairro: {agencia.endereco.bairro} </p>
                            <p>Cidade: {agencia.endereco.cidade} </p>
                            <p>Uf: {agencia.endereco.uf}</p>
                        </div>
                    </React.Fragment>
                )
            )
    }
}