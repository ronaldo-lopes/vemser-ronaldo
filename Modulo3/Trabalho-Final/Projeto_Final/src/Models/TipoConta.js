import React, {Component} from 'react';

export default class TipoConta extends Component {
    render(){
        const { tipoConta } = this.props.location.state
        return(
            <h2 key={tipoConta.id}>
                { tipoConta.nome }
            </h2>
        )
    }
}