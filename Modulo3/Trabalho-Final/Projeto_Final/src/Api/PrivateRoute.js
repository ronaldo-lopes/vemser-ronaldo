import React from 'react';
import { Route, Redirect } from 'react-router';
import { logado } from './Auth';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
        logado() ?
            <Component {...props} /> :
            <Redirect to="/" />
    )} />
)


export default PrivateRoute;