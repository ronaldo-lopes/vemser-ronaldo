import React, { Component } from 'react';
import {Route, Redirect} from 'react-router-dom';
import {logado} from '../Api/Auth';

const PublicRoute = ({component: Component, restricted, ...rest}) => {
    return (
        <Route {...rest} render={props => (
            logado() && restricted ?
                <Redirect to="/"/>
                : <Component {...props}/>
        )} />
    );
}

export default PublicRoute;