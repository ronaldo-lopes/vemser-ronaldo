const Token = 'Autorization';

export const login = () => {
    localStorage.setItem(Token,'Autorization');
}

export const logado = () => {
    if(localStorage.getItem(Token)){
        return true;
    }
    return false;
}

export const logout = () => localStorage.removeItem(Token)

