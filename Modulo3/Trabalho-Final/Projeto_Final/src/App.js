import React, {Component} from 'react';
import './Style/App.css';
import { BrowserRouter as Router, Switch } from 'react-router-dom';
import Login from './Pages/Login';
import Home from './Pages/Home';

import Agencias from './Pages/Agencias';
import Agencia from './Models/AgenciasUi';

import Clientes from './Pages/Clientes';
import Cliente from './Models/Clientes';

import TiposDeContas from './Pages/TiposContas';
import Conta from './Models/TipoConta';

import ContasClientes from './Pages/ContasClientes';
import ContaCliente from './Models/ContaCliente';

import PrivateRoute from './Api/PrivateRoute';
import PublicRoute from './Api/PublicRoute';
import { history } from './Api/History';


export default class App extends Component {// eslint-disable-next-line
  constructor(props){
    super(props)
  }
  
  render() {
    return (
      <Router history = { history }>
        <Switch>
        <PublicRoute path="/" exact component={Login}/>
        <PrivateRoute path="/home" exact component = {Home}/>
        <PrivateRoute path="/agencias" exact component = {Agencias}/>
        <PrivateRoute path="/agencia/:id" exact component = {Agencia}/>
        <PrivateRoute path="/clientes" exact component = {Clientes}/>
        <PrivateRoute path="/cliente/:id" exact component = {Cliente}/>
        <PrivateRoute path="/tiposcontas" exact component = {TiposDeContas}/>
        <PrivateRoute path="/tipoContas/:id" exact component = { Conta }/>
        <PrivateRoute path="/contasclientes" exact component = { ContasClientes }/>
        <PrivateRoute path="/conta/cliente/:id" exact component = { ContaCliente }/>

        
        </Switch>
        
        
      </Router>
    );
  }
}


