import React from "react";

export default ( props ) => 
        <React.Fragment>
            <a className= { props.class } href={ props.href }>{props.text}</a>
        </React.Fragment>