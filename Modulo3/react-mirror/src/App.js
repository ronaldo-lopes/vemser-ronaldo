import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Home from './Pages/Home';
import JsFlix from './Pages/HomeJsFlix';
import ListaAvaliacoes from './components/ListaAvaliacoes';
import TelaDetalhe from './components/TelaDetalheEpisodio';




export default class App extends Component {

  render() {
    return (
      <Router>
        
        <Route path="/" exact component={Home}/>
        <Route path="/JsFlix" component={JsFlix} />
        <Route path="/Avaliacoes" component={ListaAvaliacoes}/>
        <Route path="/episodio/:id" component={TelaDetalhe}/>
        
      </Router>
    );
  }
}


