import React, {Component} from 'react';
import '../Style/App.css';
import '../components/Button';
import ListaSeries from '../models/ListaSeries';
import Header from '../components/Header';
import Imput from '../components/Imput'

export default class HomeJsFlix extends Component {
  constructor( props ){
    super(props);
    this.listaSeries = new ListaSeries()
    console.log( this.listaSeries.invalidas() )
    this.state = {
      invalidas: null,
      filtrarAno: null,
      filtrarNome: null,
      filtrarTitulo: null,
      filtrarMedia: null,
      filtrarGenero: null,
      filtrarCreditos: null,
      filtrarSalarios: null,
      param: null,
      search: null

    }
  }

  atualizarParam( evt ) {
    this.setState({
      param: evt.target.value
    })
  }

  buscarInvalidas(){
    const invalida = this.listaSeries.invalidas();
    this.setState({
      invalidas: invalida
    })
  }

  buscarPorAno(){
    const { param } = this.state
    let filtrar = this.listaSeries.filtrarPorAno( param );
    filtrar = filtrar.map( filtro => filtro.titulo )
    this.setState({
        search: filtrar.toString()
    })
    setTimeout(() => {
      this.setState({
        search: null
      })
    }, 3000);
  }


  buscarPorNome(){
    const { param } = this.state
    let filtrar = this.listaSeries.procurarPorNome( param );
    this.setState({
      filtrarNome: filtrar.toString()
    })
    setTimeout(() => { 
      this.setState({
        filtrarNome: null
      })
    }, 3000);
  }
  
  salario(){
    const { param } = this.state
    let txt = `Informe um numero valido entre 0 e 9`
    if( param < 0 || param == null ){
      this.setState({
        filtrarSalarios: txt
      })
    } else { 
      let filtrar = this.listaSeries.totalSalarios( param );
      this.setState({
        filtrarSalarios: filtrar.toString()
      })
      setTimeout(() => {
        this.setState({
          filtrarSalarios: null
        })
      }, 3000);
    }
  }

  buscarGenero(){
    const { param } = this.state
    let filtrar = this.listaSeries.queroGenero( param );
    this.setState({
      filtrarGenero: filtrar.toString()
    })
    setTimeout(() => {
      this.setState({
        filtrarGenero: null
      })
    }, 3000);
  }

  buscarTitulo(){
    const { param } = this.state
    let filtrar = this.listaSeries.queroTitulo( param );
    this.setState({
      filtrarTitulo: filtrar.toString()
    })
    setTimeout( () => {
      this.setState({
        filtrarTitulo: null
      })
    }, 3000)
  }


  mediaEpisodios(){
    const media = this.listaSeries.mediaDeEpisodios();
    this.setState({
      filtrarMedia: media
    })
  }

  creditos(){
    const { param } = this.state
    let filtrar = this.listaSeries.creditos( param );
    this.setState({
        filtrarCreditos: filtrar.toString()
    })
    setTimeout(() => {
      this.setState({
        filtrarCreditos: null
      })
    }, 3000);
  }

  render(){
    const {invalidas,filtrarNome,filtrarSalarios,filtrarTitulo,search,filtrarMedia,filtrarGenero,filtrarCreditos} = this.state
  return (
    <div className="App">
      <Header/>
      <div>
      <ul>
        <li id="campos">
         <button className="btn azul" onClick={ this.buscarInvalidas.bind(this) }>Séries Invalidas</button>
          <p>{invalidas}</p>
        </li>
        <li id="campos">
          < Imput funcao={ this.buscarPorAno.bind(this) } valor={this.atualizarParam.bind(this)} nome= "Filtrar Por Ano"/>
          <p>{search}</p>
        </li>
        <li id="campos">
          <Imput  funcao={ this.buscarPorNome.bind(this) } valor={this.atualizarParam.bind(this)} nome="Filtrar Por Nome"/>
          <p>{filtrarNome}</p>
        </li>
        <li id="campos">
          <Imput funcao={ this.salario.bind(this) } valor={this.atualizarParam.bind(this)} nome="Buscar Salario"/>
          <p>{filtrarSalarios}</p>
        </li>
        <li id="campos">
          <Imput funcao={ this.buscarGenero.bind(this) } valor={this.atualizarParam.bind(this)} nome="Buscar Por Genero"/>
          <p>{filtrarGenero}</p>
        </li>
        <li id="campos">
          <Imput funcao={ this.buscarTitulo.bind(this) } valor={this.atualizarParam.bind(this)} nome="Buscar Por Titulo"/>
          <p>{filtrarTitulo}</p>
        </li>
        <li id="campos">
          <button className="btn azul" onClick={ this.mediaEpisodios.bind(this) } >Buscar Por Media de Episodios</button>
          <p>{filtrarMedia}</p>
        </li>
        <li id="campos">
          <Imput funcao={ this.creditos.bind(this) } valor={this.atualizarParam.bind(this)} nome="Creditos"/>
          <p>{filtrarCreditos}</p>
        </li>
      </ul>
      </div>
    </div>
  );
 }
}