

class Pokemon { // eslint-disable-line no-unused-vars
  constructor( obj ) {
    this.imgPokemon = obj.sprites.front_default;
    this.nome = obj.name;
    this.id = obj.id;
    this.pesoPokemon = obj.height;
    this.alturaPokemon = obj.weight;
    this.tipoPokemon = obj.types.map( ( type ) => `${ type.type.name }` );
    this.estatPokemon = obj.stats.map( data => data );
  }
}
