function cardapioIFood( veggie = true, comLactose = false ) {
    const cardapio = [
    'enroladinho de salsicha',
    'cuca de uva',
    'pastel de carne'
    ]
    let novoCardapio = cardapio;    
    if ( !comLactose ) {
      novoCardapio.push( 'pastel de queijo' );
    } 
    
    if ( veggie ) {
      novoCardapio.splice( 0, 1 );
      novoCardapio.splice( 1, 1 );
      novoCardapio.splice( 3, 0, 'empada de legumes marabijosa');
    }
    let resultadoFinal = []
    let i = 0;
    while (i <= novoCardapio.length - 1) {
      resultadoFinal[i] = novoCardapio[i].toUpperCase()
      i++
    }
    return resultadoFinal;
  }
  console.log(cardapioIFood( )); // esperado: [ 'CUCA DE UVA', 'PASTEL DE QUEIJO', 'EMPADA DE LEGUMES MARABIJOSA' ]

  /*
  let resultado = cardapio
                    .map(alimento => alimento.toUpperCase());
                  

      return resultado;
}
  
  */