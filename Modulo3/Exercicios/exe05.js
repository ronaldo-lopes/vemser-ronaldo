let moedas = ( function(){
    // Tudo é Privado
    function imprimirMoeda(params){
    function arredondar(numero, precisao = 2){
        const fator = Math.pow( 10, precisao);
        return Math.ceil( numero * fator ) / fator;

    }

    const {
        numero,
        separadorMilhar,
        separadorDecimal,
        colocarMoeda
    } = params

    let qtdCasasMilhares = 3 // quantidade de casa de milhar
    let stringBuffer = []
    let parteDecimal = arredondar(Math.abs(numero)%1)// arrendodar casa decimal
    let parteInteira = Math.trunc(numero)// truncate para pegar só o numero
    let parteInteiraString = Math.abs(parteInteira).toString() // transform o numero inteiro para string
    let parteInteiraTamanho = parteInteiraString.length // pega o tamanho da string

    let c = 1
    //enquanto a parte inteira for maior que 0 executa o laço
    while(parteInteiraString.length > 0){
        // verifica se c dividido por casas de milhar vai ser resto 0
        if(c % qtdCasasMilhares == 0){
            //
            stringBuffer.push( `${separadorMilhar}${parteInteiraString.slice(parteInteiraTamanho - c)}`)
            parteInteiraString = parteInteiraString.slice(0, parteInteiraTamanho - c)//diminui partindo do ponto 0

        }else if(parteInteiraString.length <= qtdCasasMilhares){
            stringBuffer.push(parteInteiraString)
            parteInteiraString = ''

        }
        c++
    }
        stringBuffer.push( parteInteiraString )

        let decimalString = parteDecimal.toString().replace('0.','').padStart(2, 0)
        const numeroFormatado = `${ stringBuffer.reverse().join('')}${separadorDecimal}${decimalString}`
        return parteInteira >= 0 ? colocarMoeda(numeroFormatado) : colocarNegativo(numeroFormatado);
    }
    
    // Tudo é Publico
    return {
        imprimirBRL: (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar:'.',
                separadorDecimal:',',
                colocarMoeda: numeroFormatado => `R$ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-${ numeroFormatado}`
            }), 
        imprimirGBP: (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar:'.',
                separadorDecimal:',',
                colocarMoeda: numeroFormatado => `£ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-£ ${ numeroFormatado}`
        }),
        imprimirFR: (numero) => 
            imprimirMoeda({
                numero,
                separadorMilhar:'.',
                separadorDecimal:',',
                colocarMoeda: numeroFormatado => `€ ${ numeroFormatado }`,
                colocarNegativo: numeroFormatado => `-€ ${ numeroFormatado}`
        }),

 }
})()

console.log(moedas.imprimirBRL(213477.0135));
console.log(moedas.imprimirGBP(213477.0135));
console.log(moedas.imprimirFR(213477.0135));