

function somarPares(valores){
    let resultado = 0;
    for(var i = 0; i < valores.length; i++){
        if(i % 2 === 0){
            resultado += valores[i];
        }
    }
    return resultado;
 
}

console.log(somarPares([1,56,4.34,6,-2]));