let circulo = {
    raio: 3,
    tipoCalculo: "A"
};



function calcularCirculo({raio, tipoCalculo:tipo}){
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow( raio, 2) : 2 * Math.PI * raio)
   }

   console.log(calcularCirculo(circulo));


