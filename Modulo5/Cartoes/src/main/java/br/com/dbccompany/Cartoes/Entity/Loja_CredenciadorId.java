package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Embeddable
public class Loja_CredenciadorId {

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "loja", 
		joinColumns = { 
				@JoinColumn(name = "id_loja_x_credenciador")},
		inverseJoinColumns = {
				@JoinColumn( name = "id_loja ")})
	private List<LojaEntity> loja = new ArrayList<>();
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "credenciador", 
		joinColumns = { 
				@JoinColumn(name = "id_loja_x_credenciador")},
		inverseJoinColumns = {
				@JoinColumn( name = "id_credenciador ")})
	private List<CredenciadorEntity> credenciador = new ArrayList<>();
	
	public Loja_CredenciadorId() {}
	
	public Loja_CredenciadorId(ArrayList<LojaEntity> loja, ArrayList<CredenciadorEntity> credenciador) {
		this.loja = loja;
		this.credenciador = credenciador;
	}
}
