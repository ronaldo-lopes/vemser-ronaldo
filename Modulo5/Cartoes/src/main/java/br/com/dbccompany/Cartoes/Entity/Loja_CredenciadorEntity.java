package br.com.dbccompany.Cartoes.Entity;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "LOJA_CREDENCIADOR")
public class Loja_CredenciadorEntity {
	
	@EmbeddedId
	private Loja_CredenciadorId id;
		
	private Float taxa;

	public Loja_CredenciadorId getId() {
		return id;
	}

	public void setId(Loja_CredenciadorId id) {
		this.id = id;
	}

	public Float getTaxa() {
		return taxa;
	}

	public void setTaxa(Float taxa) {
		this.taxa = taxa;
	}
	
	
	
}
