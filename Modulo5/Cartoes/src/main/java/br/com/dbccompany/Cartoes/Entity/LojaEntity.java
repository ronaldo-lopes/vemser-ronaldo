package br.com.dbccompany.Cartoes.Entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.dbccompany.Cartoes.Entity.Loja_CredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;

@Entity
@Table(name = "LOJA")
public class LojaEntity {

	@Id
	@SequenceGenerator( allocationSize = 1, name = "LOJA_SEQ", sequenceName = "LOJA_SEQ")
	@GeneratedValue(generator = "LOJA_SEQ", strategy = GenerationType.SEQUENCE)
	@Column( name = "ID_LOJA", nullable =  false )
	private Integer id;
	
	private String nome;
	
	@ManyToMany( mappedBy = "loja")
	private List<Loja_CredenciadorEntity> loja_Credenciador = new ArrayList<>();
	
	@OneToOne( mappedBy = "loja")
	private LancamentoEntity lancamento;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Loja_CredenciadorEntity> getLoja_Credenciador() {
		return loja_Credenciador;
	}

	public void setLoja_Credenciador(List<Loja_CredenciadorEntity> loja_Credenciador) {
		this.loja_Credenciador = loja_Credenciador;
	}
	
	
}
