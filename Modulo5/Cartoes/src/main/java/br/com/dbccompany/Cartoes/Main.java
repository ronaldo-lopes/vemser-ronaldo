package br.com.dbccompany.Cartoes;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;


import br.com.dbccompany.Cartoes.Entity.BandeiraEntity;
import br.com.dbccompany.Cartoes.Entity.CartaoEntity;
import br.com.dbccompany.Cartoes.Entity.ClienteEntity;
import br.com.dbccompany.Cartoes.Entity.CredenciadorEntity;
import br.com.dbccompany.Cartoes.Entity.EmissorEntity;
import br.com.dbccompany.Cartoes.Entity.HibernateUtil;
import br.com.dbccompany.Cartoes.Entity.LancamentoEntity;
import br.com.dbccompany.Cartoes.Entity.LojaEntity;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Session session = null;
		Transaction transaction = null;
		try {
			session = HibernateUtil.getSessionFactory().openSession();
			transaction = session.beginTransaction();
			
			LojaEntity loja = new LojaEntity();
			loja.setNome("Centauro");
			
			List<LojaEntity> lojas = new ArrayList<>();
			lojas.add(loja);
			
			ClienteEntity cliente = new ClienteEntity();
			cliente.setNome("Pedro");
			
			LancamentoEntity lancamento = new LancamentoEntity();
			lancamento.setDescricao("Mês Abril");
			lancamento.setValor(500.00f);
			
			BandeiraEntity bandeira = new BandeiraEntity();
			bandeira.setNome("Visa");
			
			CredenciadorEntity credenciador = new CredenciadorEntity();
			credenciador.setNome("banco");
			
			List<CredenciadorEntity> credenciar = new ArrayList<>();
			credenciar.add(credenciador);
			
			CartaoEntity cartao = new CartaoEntity();
			cartao.setBandeira(bandeira);
			
			EmissorEntity emissor = new EmissorEntity();
			emissor.setNome("Banco");
			emissor.setTaxa(20.00f);
			
			session.save(cliente);
			session.save(loja);
			session.save(lancamento);
			session.save(bandeira);
			session.save(credenciador);
			session.save(cartao);
			session.save(emissor);
			
		
			
			transaction.commit();
		}catch(Exception e) {
			if( transaction != null) {
				transaction.rollback();
			}
			 System.exit(1);
		}finally {
			 System.exit(0);
		}
		
	}

}
