package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ELFO_VERDE")
public class Elfo_VerdeEntity extends ElfoEntity {
	
	public Elfo_VerdeEntity() {
		super.setTipo(Tipo.ELFO_VERDE);
		super.setQtdExperienciaPorAtaque(2);
	}

}
