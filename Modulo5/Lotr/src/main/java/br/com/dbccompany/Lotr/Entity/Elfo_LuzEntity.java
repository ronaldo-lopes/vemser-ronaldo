package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "ELFO_LUZ")
public class Elfo_LuzEntity extends PersonagemEntity {
	
	public Elfo_LuzEntity() {
		super.setTipo(Tipo.ELFO_LUZ);
		super.setVida(100.0);
	}

}
