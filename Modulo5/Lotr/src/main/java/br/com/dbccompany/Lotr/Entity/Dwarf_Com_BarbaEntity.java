package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table( name = "DWARF_COM_BARBA")
public class Dwarf_Com_BarbaEntity extends PersonagemEntity {

	public Dwarf_Com_BarbaEntity() {
		super.setTipo(Tipo.DWARF_COM_BARBA);
		super.setVida(110.0);
	}
}
