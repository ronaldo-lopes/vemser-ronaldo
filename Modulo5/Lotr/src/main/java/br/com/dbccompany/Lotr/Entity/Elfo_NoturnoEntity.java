package br.com.dbccompany.Lotr.Entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table( name = "ELFO_NOTURNO")
public class Elfo_NoturnoEntity extends PersonagemEntity {
	
	public Elfo_NoturnoEntity() {
		super.setTipo(Tipo.ELFO_NOTURNO);
		super.setVida(100.0);
	}
}
