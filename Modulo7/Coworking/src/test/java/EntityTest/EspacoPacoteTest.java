package EntityTest;

import br.com.dbccompany.Coworking.Entity.Espaco;
import br.com.dbccompany.Coworking.Entity.Espaco_Pacote;
import br.com.dbccompany.Coworking.Entity.TipoContratacao;
import br.com.dbccompany.Coworking.Service.EspacoPacoteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


public class EspacoPacoteTest {
    @Autowired
    EspacoPacoteService espacoPacoteService = new EspacoPacoteService();

    @Test
    public void testGetEspaco(){
        Espaco_Pacote espacoPacote = new Espaco_Pacote();
        Espaco espaco = new Espaco();
        espaco.setId(1);
        espacoPacote.setEspaco(espaco);

        Assertions.assertEquals(1, espacoPacote.getEspaco().getId());

    }

    @Test
    public void testGetTipoContratacao(){
        Espaco_Pacote espacoPacote = new Espaco_Pacote();
        espacoPacote.setTipoContratacao(TipoContratacao.HORA);

        Assertions.assertEquals(TipoContratacao.HORA, espacoPacote.getTipoContratacao());

    }

    @Test
    public void testGetQuantidade(){
        Espaco_Pacote espacoPacote = new Espaco_Pacote();
        espacoPacote.setQuantidade(5);

        Assertions.assertEquals(5, espacoPacote.getQuantidade());

    }

    @Test
    public void testGetPrazo(){
        Espaco_Pacote espacoPacote = new Espaco_Pacote();
        espacoPacote.setPrazo(2);

        Assertions.assertEquals(2,espacoPacote.getPrazo());

    }
}
