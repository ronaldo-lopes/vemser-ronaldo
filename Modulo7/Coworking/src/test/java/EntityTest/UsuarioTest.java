package EntityTest;

import br.com.dbccompany.Coworking.Entity.Usuario;
import br.com.dbccompany.Coworking.Service.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


public class UsuarioTest {

    @Autowired
    private UsuarioService usuarioService;

    @Test
    public void testGetNome(){
        Usuario usuario = new Usuario();
        usuario.setNome("Ronaldo");

        Assertions.assertEquals("Ronaldo", usuario.getNome());


    }

    @Test
    public void testGetEmail(){
        Usuario usuario = new Usuario();
        usuario.setEmail("rlsanlo@gmail.com");

        Assertions.assertEquals("rlsanlo@gmail.com", usuario.getEmail());


    }

    @Test
    public void testGetLogin(){
        Usuario usuario = new Usuario();
        usuario.setLogin("rlsanlo");

        Assertions.assertEquals("rlsanlo",usuario.getLogin());

    }

    @Test
    public void testGetSenha() {
        Usuario usuario = new Usuario();
        usuario.setSenha("1909!*");

        Assertions.assertEquals("1909!*", usuario.getSenha());

    }
}
