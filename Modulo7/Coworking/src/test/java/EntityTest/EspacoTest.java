package EntityTest;

import br.com.dbccompany.Coworking.Entity.Espaco;
import br.com.dbccompany.Coworking.Service.EspacoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


public class EspacoTest {
    @Autowired
    EspacoService espacoService = new EspacoService();

    @Test
    public void testGetNome(){
        Espaco espaco = new Espaco();
        espaco.setNome("Sala Comercial");

        Assertions.assertEquals("Sala Comercial", espaco.getNome());

    }

    @Test
    public void testGetQtdPessoas(){
        Espaco espaco = new Espaco();
        espaco.setQtdPessoas(40);

        Assertions.assertEquals(40, espaco.getQtdPessoas());

    }

    @Test
    public void testGetValorEspaco(){
        Espaco espaco = new Espaco();
        espaco.setValor(200.00);

        Assertions.assertEquals(200.00,espaco.getValor());

    }
}
