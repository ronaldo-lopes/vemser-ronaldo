package EntityTest;

import br.com.dbccompany.Coworking.Entity.Pacote;
import br.com.dbccompany.Coworking.Service.PacoteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


public class PacoteTest {

    @Autowired
    PacoteService pacoteService = new PacoteService();

    @Test
    public void testGetValor(){
        Pacote pacote = new Pacote();
        pacote.setValor(120.00);

        Assertions.assertEquals(120.00, pacote.getValor());

    }
}
