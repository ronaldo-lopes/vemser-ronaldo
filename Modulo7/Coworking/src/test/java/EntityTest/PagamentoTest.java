package EntityTest;

import br.com.dbccompany.Coworking.Entity.Cliente_Pacote;
import br.com.dbccompany.Coworking.Entity.Pagamento;
import br.com.dbccompany.Coworking.Entity.TipoPagamento;
import br.com.dbccompany.Coworking.Service.PagamentoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


public class PagamentoTest {
    @Autowired
    PagamentoService pagamentoService = new PagamentoService();

    @Test
    public void testGetClientePacote(){
        Pagamento pagamento = new Pagamento();
        Cliente_Pacote clientePacote = new Cliente_Pacote();
        clientePacote.setId(1);
        pagamento.setClientePacote( clientePacote );

        Assertions.assertEquals(1,pagamento.getClientePacote().getId());

    }

    @Test
    public void testGetTipoPagamento(){
        Pagamento pagamento = new Pagamento();
        pagamento.setTipoPagamento(TipoPagamento.DEBITO);

        Assertions.assertEquals(TipoPagamento.DEBITO, pagamento.getTipoPagamento());
        this.pagamentoService.salvar(pagamento);
        this.pagamentoService.findByTipoPagamento(pagamento.getTipoPagamento());
    }
}
