package EntityTest;

import br.com.dbccompany.Coworking.Entity.Tipo_Contato;
import br.com.dbccompany.Coworking.Service.TipoContatoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


public class TipoContatoTest {

    @Autowired
    TipoContatoService tipoContatoService = new TipoContatoService();

    @Test
    public void testGetNome(){
        Tipo_Contato tipoContato = new Tipo_Contato();
        tipoContato.setNome("email");

        Assertions.assertEquals("email", tipoContato.getNome());

    }
}
