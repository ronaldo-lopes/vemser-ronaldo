package EntityTest;

import br.com.dbccompany.Coworking.Entity.Contato;
import br.com.dbccompany.Coworking.Entity.Tipo_Contato;
import br.com.dbccompany.Coworking.Service.ContatoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


public class ContatoTest {
    @Autowired
    ContatoService contatoService = new ContatoService();

    @Test
    public void testGetTipoContato(){
        Contato contato = new Contato();
        Tipo_Contato tipoContato = new Tipo_Contato();
        tipoContato.setNome("email");
        contato.setTipoContato(tipoContato);

        Assertions.assertEquals("email", contato.getTipoContato().getNome());

    }

    @Test
    public void testGetValor(){
        Contato contato = new Contato();
        contato.setValor(40.00);

        Assertions.assertEquals(40.00,contato.getValor());

    }
}
