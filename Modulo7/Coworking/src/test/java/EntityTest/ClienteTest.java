package EntityTest;

import br.com.dbccompany.Coworking.Entity.Cliente;
import br.com.dbccompany.Coworking.Service.ClienteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


public class ClienteTest {
    @Autowired
    ClienteService clienteService = new ClienteService();

    @Test
    public void testGetNomeCliente(){
        Cliente cliente = new Cliente();
        cliente.setNome("Ronaldo");

        Assertions.assertEquals("Ronaldo",cliente.getNome() );

    }

    @Test
    public void testGetCpf(){
        Cliente cliente = new Cliente();
        cliente.setCpf("003.004.005-02");

        Assertions.assertEquals("003.004.005-02",cliente.getCpf());
    }

}
