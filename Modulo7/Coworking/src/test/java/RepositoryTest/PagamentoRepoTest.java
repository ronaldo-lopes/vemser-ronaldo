package RepositoryTest;

import br.com.dbccompany.Coworking.Entity.Pagamento;
import br.com.dbccompany.Coworking.Entity.TipoPagamento;
import br.com.dbccompany.Coworking.Repository.PagamentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PagamentoRepoTest {
    @Autowired
    PagamentoRepository repository;

    @Test
    public void findByTipoPagamentoTest(){
        TipoPagamento tipoPag = null;
        Pagamento pagamento1 = new Pagamento();
        Pagamento pagamento2 = new Pagamento();

        pagamento1.setTipoPagamento(tipoPag.DEBITO);
        repository.save(pagamento1);

        pagamento2.setTipoPagamento(tipoPag.CREDITO);
        repository.save(pagamento2);

        Assertions.assertEquals(tipoPag.DEBITO,repository.findByTipoPagamento(pagamento1.getTipoPagamento()));
        Assertions.assertEquals(tipoPag.CREDITO,repository.findByTipoPagamento(pagamento2.getTipoPagamento()));
    }
}
