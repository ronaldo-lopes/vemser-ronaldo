package RepositoryTest;

import br.com.dbccompany.Coworking.Entity.Tipo_Contato;
import br.com.dbccompany.Coworking.Repository.TipoContatoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TipoContatoRepoTest {

    @Autowired
    TipoContatoRepository repository;

    @Test
    public void findByNomeTest(){
        Tipo_Contato tipoContato1 = new Tipo_Contato();
        Tipo_Contato tipoContato2 = new Tipo_Contato();

        tipoContato1.setNome("Lopes");
        repository.save(tipoContato1);

        tipoContato2.setNome("Santos");
        repository.save(tipoContato2);

        Assertions.assertEquals("Lopes", repository.findByNome("Lopes"));
        Assertions.assertEquals("Santos", repository.findByNome("Santos"));
    }
}
