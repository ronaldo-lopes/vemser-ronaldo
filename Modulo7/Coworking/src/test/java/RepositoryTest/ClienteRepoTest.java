package RepositoryTest;

import br.com.dbccompany.Coworking.Entity.Cliente;
import br.com.dbccompany.Coworking.Repository.ClienteRepository;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClienteRepoTest {
    @Autowired
    ClienteRepository repository;

    @Test
    public void findByNomeClienteTest(){
        Cliente cliente1 = new Cliente();
        Cliente cliente2 = new Cliente();

        cliente1.setId(null);
        cliente1.setNome("Ronaldo");
        this.repository.save(cliente1);

        cliente2.setId(null);
        cliente2.setNome("Deborah");
        this.repository.save(cliente2);

        assertEquals("Ronaldo", repository.findById(1).get().getNome());
        assertEquals("Deborah", repository.findById(2).get().getNome());

    }

    @Test
    public void findByCpfClienteTest(){
        Cliente cliente1 = new Cliente();
        Cliente cliente2 = new Cliente();

        cliente1.setCpf("003.004.005.23");
        repository.save(cliente1);

        cliente2.setCpf("002.001.003-12");
        repository.save(cliente2);

        assertEquals("003.004.005.23",repository.findByCpf(cliente1.getCpf()));
        assertEquals("002.001.003-12",repository.findByCpf(cliente2.getCpf()));
    }


}
