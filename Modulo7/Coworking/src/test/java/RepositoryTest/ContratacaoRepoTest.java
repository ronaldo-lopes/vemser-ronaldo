package RepositoryTest;

import br.com.dbccompany.Coworking.Entity.Contratacao;
import br.com.dbccompany.Coworking.Entity.TipoContratacao;
import br.com.dbccompany.Coworking.Repository.ContratacaoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ContratacaoRepoTest {
    @Autowired
    ContratacaoRepository repository;

    @Test
    public void findByTipoContratacaoTest(){
        TipoContratacao tipoContratacao = null;
        Contratacao contratacao1 = new Contratacao();
        Contratacao contratacao2 = new Contratacao();

        contratacao1.setTipoContratacao(tipoContratacao.HORA);
        repository.save(contratacao1);

        contratacao2.setTipoContratacao(tipoContratacao.DIARIA);
        repository.save(contratacao2);

        Assertions.assertEquals(tipoContratacao.HORA,repository.findByTipoContratacao(contratacao1.getTipoContratacao()));
        Assertions.assertEquals(tipoContratacao.DIARIA,repository.findByTipoContratacao(contratacao2.getTipoContratacao()));
    }

    @Test
    public void findByQuantidadeContratacaoTest(){
        Contratacao contratacao1 = new Contratacao();
        Contratacao contratacao2 = new Contratacao();

        contratacao1.setQuantidade(10);
        repository.save(contratacao1);

        contratacao2.setQuantidade(5);
        repository.save(contratacao2);

        Assertions.assertEquals(10,repository.findByQuantidade(contratacao1.getQuantidade()));
        Assertions.assertEquals(5,repository.findByQuantidade(contratacao2.getQuantidade()));
    }

    @Test
    public void findByDescontoContratacaoTest(){
        Contratacao contratacao1 = new Contratacao();
        Contratacao contratacao2 = new Contratacao();

        contratacao1.setDesconto(5.00);
        repository.save(contratacao1);

        contratacao2.setDesconto(4.00);
        repository.save(contratacao2);

        Assertions.assertEquals(5.00,repository.findByDesconto(contratacao1.getDesconto()));
        Assertions.assertEquals(4.00,repository.findByDesconto(contratacao2.getDesconto()));
    }

    @Test
    public void findByPrazoContratacaoTest(){
        Contratacao contratacao1 = new Contratacao();
        Contratacao contratacao2 = new Contratacao();

        contratacao1.setPrazo(2);
        repository.save(contratacao1);

        contratacao2.setPrazo(4);
        repository.save(contratacao2);

        Assertions.assertEquals(2,repository.findByPrazo(contratacao1.getPrazo()));
        Assertions.assertEquals(4,repository.findByPrazo(contratacao2.getPrazo()));
    }
}
