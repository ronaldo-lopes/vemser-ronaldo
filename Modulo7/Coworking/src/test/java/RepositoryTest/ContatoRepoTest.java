package RepositoryTest;

import br.com.dbccompany.Coworking.Entity.Contato;
import br.com.dbccompany.Coworking.Repository.ContatoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ContatoRepoTest {
    @Autowired
    ContatoRepository repository;

    @Test
    public void findByValorContatoTest(){
        Contato contato1 = new Contato();
        Contato contato2 = new Contato();

        contato1.setValor(20.00);
        repository.save(contato1);

        contato2.setValor(10.00);
        repository.save(contato2);

        Assertions.assertEquals(20.00, repository.findByValor(contato1.getValor()));
        Assertions.assertEquals(10.00, repository.findByValor(contato2.getValor()));
    }
}
