package RepositoryTest;

import br.com.dbccompany.Coworking.Entity.Usuario;
import br.com.dbccompany.Coworking.Repository.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UsuarioRepoTest {
    @Autowired
    UsuarioRepository repository;

    @Test
    public void findByNomeTest(){
        Usuario usuario1 = new Usuario();
        Usuario usuario2 = new Usuario();

        usuario1.setNome("Joao");
        repository.save(usuario1);

        usuario2.setNome("Pedro");
        repository.save(usuario2);

        Assertions.assertEquals("Joao", repository.findByNome(usuario1.getNome()));
        Assertions.assertEquals("Pedro", repository.findByNome(usuario2.getNome()));
    }

    @Test
    public void findByEmailTest(){
        Usuario usuario1 = new Usuario();
        Usuario usuario2 = new Usuario();

        usuario1.setEmail("joaozinho@gmail.com");
        repository.save(usuario1);

        usuario2.setEmail("pedrinho@gmail.com");
        repository.save(usuario2);

        Assertions.assertEquals("joaozinho@gmial.com", repository.findByEmail(usuario1.getEmail()));
        Assertions.assertEquals("pedrinho@gmail.com", repository.findByEmail(usuario2.getEmail()));
    }

    @Test
    public void findByLoginTest(){
        Usuario usuario1 = new Usuario();
        Usuario usuario2 = new Usuario();

        usuario1.setLogin("joao123");
        repository.save(usuario1);

        usuario2.setLogin("pedro123");
        repository.save(usuario2);

        Assertions.assertEquals("joao123", repository.findByLogin(usuario1.getLogin()));
        Assertions.assertEquals("pedro123", repository.findByLogin(usuario2.getLogin()));
    }
}
