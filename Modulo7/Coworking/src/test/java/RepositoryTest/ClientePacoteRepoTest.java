package RepositoryTest;

import br.com.dbccompany.Coworking.Entity.Cliente_Pacote;
import br.com.dbccompany.Coworking.Repository.ClientePacoteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


public class ClientePacoteRepoTest {
    @Autowired
    ClientePacoteRepository repository;

    @Test
    public void findByQuantidadeClientePacoteTest(){
        Cliente_Pacote clientePacote1 = new Cliente_Pacote();
        Cliente_Pacote clientePacote2 = new Cliente_Pacote();

        clientePacote1.setQuantidade(3);
        repository.save(clientePacote1);

        clientePacote2.setQuantidade(5);
        repository.save(clientePacote2);

        Assertions.assertEquals(3, repository.findByQuantidade(clientePacote1.getQuantidade()));
        Assertions.assertEquals(5, repository.findByQuantidade(clientePacote2.getQuantidade()));
    }
}
