package RepositoryTest;

import br.com.dbccompany.Coworking.Entity.Espaco;
import br.com.dbccompany.Coworking.Repository.EspacoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EspacoRepoTest {
    @Autowired
    EspacoRepository repository;

    @Test
    public void findByNomeEspacoTest(){
        Espaco espaco1 = new Espaco();
        Espaco espaco2 = new Espaco();

        espaco1.setNome("WeWork");
        repository.save(espaco1);

        espaco2.setNome("Ufo");
        repository.save(espaco2);

        Assertions.assertEquals("WeWork", repository.findByNome(espaco1.getNome()));
        Assertions.assertEquals("Ufo", repository.findByNome(espaco2.getNome()));
    }

    @Test
    public void findByQtdPessoasEspacoTest(){
        Espaco espaco1 = new Espaco();
        Espaco espaco2 = new Espaco();

        espaco1.setQtdPessoas(40);
        repository.save(espaco1);

        espaco2.setQtdPessoas(20);
        repository.save(espaco2);

        Assertions.assertEquals(40,repository.findByQtdPessoas(espaco1.getQtdPessoas()));
        Assertions.assertEquals(20,repository.findByQtdPessoas(espaco2.getQtdPessoas()));
    }

    @Test
    public void findByValorEspacoTest(){
        Espaco espaco1 = new Espaco();
        Espaco espaco2 = new Espaco();

        espaco1.setValor(2.000);
        repository.save(espaco1);

        espaco2.setValor(1.000);
        repository.save(espaco2);

        Assertions.assertEquals(2.000, repository.findByValor(espaco1.getValor()));
        Assertions.assertEquals(1.000, repository.findByValor(espaco1.getValor()));
    }
}
