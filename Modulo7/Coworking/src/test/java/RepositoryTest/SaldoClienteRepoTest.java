package RepositoryTest;

import br.com.dbccompany.Coworking.Entity.SaldoCliente;
import br.com.dbccompany.Coworking.Entity.TipoContratacao;
import br.com.dbccompany.Coworking.Repository.SaldoClienteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SaldoClienteRepoTest {
    @Autowired
    SaldoClienteRepository repository;

    @Test
    public void findByTipoContratacaoTest(){
        TipoContratacao tipoContratacao = null;
        SaldoCliente saldoCliente1 = new SaldoCliente();
        SaldoCliente saldoCliente2 = new SaldoCliente();

        saldoCliente1.setTipoContratacao(tipoContratacao.HORA);
        repository.save(saldoCliente1);

        saldoCliente2.setTipoContratacao(tipoContratacao.DIARIA);
        repository.save(saldoCliente2);

        Assertions.assertEquals(tipoContratacao.HORA,repository.findByTipoContratacao(saldoCliente1.getTipoContratacao()));
        Assertions.assertEquals(tipoContratacao.DIARIA, repository.findByTipoContratacao(saldoCliente2.getTipoContratacao()));
    }

    @Test
    public void findByQuantidadeTest(){
        SaldoCliente saldoCliente1 = new SaldoCliente();
        SaldoCliente saldoCliente2 = new SaldoCliente();

        saldoCliente1.setQuantidade(5);
        repository.save(saldoCliente1);

        saldoCliente2.setQuantidade(2);
        repository.save(saldoCliente2);

        Assertions.assertEquals(5,repository.findByQuantidade(saldoCliente1.getQuantidade()));
        Assertions.assertEquals(2,repository.findByQuantidade(saldoCliente2.getQuantidade()));
    }

}
