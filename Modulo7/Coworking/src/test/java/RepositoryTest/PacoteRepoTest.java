package RepositoryTest;

import br.com.dbccompany.Coworking.Entity.Pacote;
import br.com.dbccompany.Coworking.Repository.PacoteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PacoteRepoTest {
    @Autowired
    PacoteRepository repository;

    @Test
    public void findByValorPacoteTest(){
        Pacote pacote1 = new Pacote();
        Pacote pacote2 = new Pacote();

        pacote1.setValor(400.00);
        repository.save(pacote1);

        pacote2.setValor(250.00);
        repository.save(pacote2);

        Assertions.assertEquals(400.00,repository.findByValor(pacote1.getValor()));
        Assertions.assertEquals(250.00,repository.findByValor(pacote2.getValor()));
    }
}
