package ServiceTest;

import br.com.dbccompany.Coworking.Entity.Contato;
import br.com.dbccompany.Coworking.Entity.Tipo_Contato;
import br.com.dbccompany.Coworking.Service.ContatoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ContatoServiceTest {
    @Autowired
    ContatoService service;

    @Test
    public void salvarContatoTest(){
        Tipo_Contato tipo1 = new Tipo_Contato();
        tipo1.setNome("Douglas");
        Contato contato1 = new Contato();
        contato1.setTipoContato(tipo1);
        service.salvar(contato1);

        Assertions.assertEquals("Douglas",service.salvar(contato1));
    }

    @Test
    public void editarContatoTet(){
        Tipo_Contato tipo1 = new Tipo_Contato();
        tipo1.setId(40);
        Contato contato1 = new Contato();
        contato1.setTipoContato(tipo1);
        service.editar(contato1,tipo1.getId());

        Assertions.assertEquals(40,service.editar(contato1,contato1.getTipoContato().getId()));
    }

    @Test
    public void deleteContatoTest(){
        Contato contato1 = new Contato();
        contato1.setId(40);
        service.delete(contato1.getId());
    }
}
