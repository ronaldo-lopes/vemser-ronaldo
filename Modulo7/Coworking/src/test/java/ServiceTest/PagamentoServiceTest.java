package ServiceTest;

import br.com.dbccompany.Coworking.Entity.Pagamento;
import br.com.dbccompany.Coworking.Entity.TipoPagamento;
import br.com.dbccompany.Coworking.Service.PagamentoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PagamentoServiceTest {
    @Autowired
    PagamentoService service;

    @Test
    public void salvarPagamentoTest(){
        TipoPagamento tipoPagamento1 = null;
        Pagamento pagamento1 = new Pagamento();
        pagamento1.setTipoPagamento(tipoPagamento1.DEBITO);
        service.salvar(pagamento1);

        Assertions.assertEquals(tipoPagamento1.DEBITO, service.salvar(pagamento1));
    }

    @Test
    public void editarPagamentoTest(){
        TipoPagamento tipoPagamento1 = null;
        Pagamento pagamento1 = new Pagamento();
        pagamento1.setTipoPagamento(tipoPagamento1.CREDITO);
        service.editar(pagamento1,20);

        Assertions.assertEquals(20,service.editar(pagamento1,pagamento1.getId()));
    }

    @Test
    public void deletarPagamentoTest(){
        Pagamento pagamento1 = new Pagamento();
        pagamento1.setId(20);
        service.delete(pagamento1.getId());
    }
}
