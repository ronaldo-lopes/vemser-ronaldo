package ServiceTest;

import br.com.dbccompany.Coworking.Entity.Cliente;
import br.com.dbccompany.Coworking.Service.ClienteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ClienteServiceTest {
    @Autowired
    ClienteService service;

    @Test
    public void salvarClienteTest(){
        Cliente cliente1 = new Cliente();
        cliente1.setNome("Ronaldo");
        service.salvar(cliente1);

        Assertions.assertEquals("Ronaldo", service.salvar(cliente1));
    }

    @Test
    public void editarClienteTest(){
        Cliente cliente1 = new Cliente();
        cliente1.setNome("joao");
        cliente1.setId(5);
        service.editar(cliente1,5);

        Assertions.assertEquals(5,service.editar(cliente1,cliente1.getId()));
    }

    @Test
    public void deleteClienteTest(){
        Cliente cliente1 = new Cliente();
        cliente1.setId(4);
        service.delete(cliente1.getId());

    }
}
