package ServiceTest;

import br.com.dbccompany.Coworking.Entity.Espaco;
import br.com.dbccompany.Coworking.Service.EspacoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class EspacoServiceTest {
    @Autowired
    EspacoService service;

    @Test
    public void salvarEspacoTest(){
        Espaco espaco1 = new Espaco();
        espaco1.setNome("WeWork");
        service.salvar(espaco1);

        Assertions.assertEquals("WeWork", service.salvar(espaco1));
    }

    @Test
    public void editarEspacoTest(){
        Espaco espaco1 = new Espaco();
        espaco1.setQtdPessoas(40);
        service.editar(espaco1,45);

        Assertions.assertEquals(45,service.editar(espaco1,espaco1.getId()));
    }

    @Test
    public void deleteEspacoTest(){
        Espaco espaco1 = new Espaco();
        espaco1.setId(45);
        service.delete(espaco1.getId());
    }
}
