package ServiceTest;

import br.com.dbccompany.Coworking.Entity.Usuario;
import br.com.dbccompany.Coworking.Service.UsuarioService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UsuarioServiceTest {
    @Autowired
    UsuarioService service;

    @Test
    public void salvarUsuarioTest(){
        Usuario usuario1 = new Usuario();
        usuario1.setNome("Ronaldo");
        service.salvar(usuario1);

        Assertions.assertEquals("Ronaldo", service.salvar(usuario1));
    }

    @Test
    public void editarUsuarioTest(){
        Usuario usuario1 = new Usuario();
        usuario1.setNome("Pedro");
        usuario1.setId(20);
        service.editar(usuario1,20);

        Assertions.assertEquals(20,service.editar(usuario1,usuario1.getId()));
    }

    @Test
    public void deleteUsuarioTest(){
        Usuario usuario1 = new Usuario();
        usuario1.setId(20);
        service.delete(usuario1.getId());
    }
}
