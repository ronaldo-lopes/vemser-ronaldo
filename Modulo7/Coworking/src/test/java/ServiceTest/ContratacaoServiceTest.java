package ServiceTest;

import br.com.dbccompany.Coworking.Entity.Contratacao;
import br.com.dbccompany.Coworking.Entity.TipoContratacao;
import br.com.dbccompany.Coworking.Service.ContratacaoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ContratacaoServiceTest {
    @Autowired
    ContratacaoService service;

    @Test
    public void salvarContratacaoTest(){
        TipoContratacao tipoContratacao1 = null;
        Contratacao contratacao1 = new Contratacao();
        contratacao1.setTipoContratacao(tipoContratacao1.DIARIA);
        service.salvar(contratacao1);

        Assertions.assertEquals(tipoContratacao1.DIARIA, service.salvar(contratacao1));
    }

    @Test
    public void editarContratacaoTest(){
        TipoContratacao tipoContratacao1 = null;
        Contratacao contratacao1 = new Contratacao();
        contratacao1.setTipoContratacao(tipoContratacao1.HORA);
        service.editar(contratacao1,5);

        Assertions.assertEquals(5,service.editar(contratacao1,contratacao1.getId()));
    }

    @Test
    public void deleteContratacaoTest(){
        Contratacao contratacao1 = new Contratacao();
        contratacao1.setId(5);
        service.delete(contratacao1.getId());
    }
}
