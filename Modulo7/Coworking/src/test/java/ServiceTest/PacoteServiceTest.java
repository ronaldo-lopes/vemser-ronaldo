package ServiceTest;

import br.com.dbccompany.Coworking.Entity.Pacote;
import br.com.dbccompany.Coworking.Service.PacoteService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;


@SpringBootTest
public class PacoteServiceTest {
    @Autowired
    PacoteService service;

    @Test
    public void salvarPacoteTest(){
        Pacote pacote1 = new Pacote();
        pacote1.setValor(300.00);
        service.salvar(pacote1);

        Assertions.assertEquals(300.00,service.salvar(pacote1));
    }

    @Test
    public void editarPacoteTest(){
        Pacote pacote1 = new Pacote();
        pacote1.setValor(400.00);
        service.editar(pacote1,5);

        Assertions.assertEquals(5,service.editar(pacote1,pacote1.getId()));
    }

    @Test
    public void deletarPacoteTest(){
        Pacote pacote1 = new Pacote();
        pacote1.setId(5);
        service.delete(pacote1.getId());
    }
}
