package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.Contratacao;
import br.com.dbccompany.Coworking.Entity.Espaco;
import br.com.dbccompany.Coworking.Entity.TipoContratacao;
import br.com.dbccompany.Coworking.Repository.ContratacaoRepository;
import br.com.dbccompany.Coworking.Repository.EspacoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContratacaoService {
    @Autowired
    private EspacoRepository Espacorepository;
    private ContratacaoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Contratacao salvar(Contratacao contratacao) {
        return repository.save(contratacao);
    }

    @Transactional(rollbackFor = Exception.class)
    public Contratacao editar(Contratacao contratacao, Integer id) {
        contratacao.setId(id);
        return repository.save(contratacao);
    }

    public List<Contratacao> todasContratacoes() {
        return repository.findAll();
    }

    public Contratacao contratacaoEspecifica(Integer id) {
        Optional<Contratacao> contratacao = repository.findById(id);
        return contratacao.get();
    }

    public Contratacao findByTipoContratacao(TipoContratacao tipoContratacao){
        return repository.findByTipoContratacao( tipoContratacao );
    }

    public Double valorDeContratacao( Integer espacosID, Integer contratacaoID){
        Optional<Espaco> espaco = Espacorepository.findById(espacosID);
        Optional<Contratacao> contratacao = repository.findById(contratacaoID);
        return espaco.get().getValor() * contratacao.get().getQuantidade();
    }


    public Contratacao findByQuantidade(Integer quantidade){
        return repository.findByQuantidade(quantidade);
    }

    public Contratacao findByDesconto(Double desconto){
        return repository.findByDesconto(desconto);
    }

    public Contratacao findByPrazo(Integer prazo){
        return repository.findByPrazo( prazo );
    }



    public void delete( Integer id){
        repository.deleteById( id );
    }
}
