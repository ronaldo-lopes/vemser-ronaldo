package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.Acesso;
import br.com.dbccompany.Coworking.Service.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/api/acesso")
public class AcessoControler {
    @Autowired
    AcessoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Acesso> todosAcessos() {

        return service.todosAcessos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Acesso novoAcesso(@RequestBody Acesso acesso) {

        return service.salvar(acesso);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Acesso editarAcesso(@PathVariable Integer id, @RequestBody Acesso acesso) {
        return service.editar(acesso, id);
    }

    @GetMapping(value = "/data/{data}")
    @ResponseBody
    public Acesso buscarPorData(@PathVariable Date data){
        return service.findByDate( data );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete(@PathVariable Integer id){
        service.delete( id );
        return true;
    }
}
