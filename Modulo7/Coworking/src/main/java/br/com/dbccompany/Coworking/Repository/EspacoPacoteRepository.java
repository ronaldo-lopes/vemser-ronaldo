package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.Espaco_Pacote;
import br.com.dbccompany.Coworking.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EspacoPacoteRepository extends CrudRepository<Espaco_Pacote, Integer> {
    Espaco_Pacote findByTipoContratacao(TipoContratacao tipoContratacao);

    Espaco_Pacote findByQuantidade(Integer quantidade);

    Espaco_Pacote findByPrazo(Integer prazo);

    List<Espaco_Pacote> findAll();
}
