package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.Cliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
    Cliente findByNome(String nome);
    Cliente findByCpf(String cpf);
    Cliente findByDataNascimento(Date date);
    List<Cliente> findAll();

}
