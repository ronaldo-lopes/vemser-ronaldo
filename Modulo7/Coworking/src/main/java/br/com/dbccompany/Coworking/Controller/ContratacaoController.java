package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.Contratacao;
import br.com.dbccompany.Coworking.Service.ContratacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {
    @Autowired
    ContratacaoService service;

    @GetMapping(value = "/todas")
    @ResponseBody
    public List<Contratacao> todasContratacoes() {

        return service.todasContratacoes();
    }

    @PostMapping(value = "/nova")
    @ResponseBody
    public Contratacao novaContratacao(@RequestBody Contratacao contratacao) {

        return service.salvar(contratacao);
    }

    @PostMapping( value = "/orcamentoContratacao")
    @ResponseBody
    public Double novoOrcamento(@RequestBody Integer espacoId, @RequestBody Integer contratacaoId){
        return service.valorDeContratacao(espacoId,contratacaoId);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Contratacao editarContratacao(@PathVariable Integer id, @RequestBody Contratacao contratacao) {
        return service.editar(contratacao, id);
    }

    @GetMapping(value = "/quantidade/{quantidade}")
    @ResponseBody
    public Contratacao buscarQuantidadeContratacao( Integer quantidade ){
        return service.findByQuantidade( quantidade );
    }

    @GetMapping(value = "/desconto/{desconto}")
    @ResponseBody
    public Contratacao buscarPorDesconto( Double desconto ){
        return service.findByDesconto(desconto);
    }

    @GetMapping(value = "/prazo/{prazo}")
    @ResponseBody
    public Contratacao buscarPorPrazo( Integer prazo ){
        return service.findByPrazo(prazo);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( Integer id){
        service.delete(id);
        return true;
    }
}
