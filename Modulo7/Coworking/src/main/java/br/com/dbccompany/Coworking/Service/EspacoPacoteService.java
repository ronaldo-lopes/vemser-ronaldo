package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.Espaco_Pacote;
import br.com.dbccompany.Coworking.Entity.TipoContratacao;
import br.com.dbccompany.Coworking.Repository.EspacoPacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class EspacoPacoteService {
    @Autowired
    private EspacoPacoteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Espaco_Pacote salvar(Espaco_Pacote espacoPacote) {
        return repository.save(espacoPacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public Espaco_Pacote editar(Espaco_Pacote espacoPacote, Integer id) {
        espacoPacote.setId(id);
        return repository.save(espacoPacote);
    }

    public List<Espaco_Pacote> todosEspacosPacotes() {
        return repository.findAll();
    }

    public Espaco_Pacote espaco_pacoteEspecifico(Integer id) {
        Optional<Espaco_Pacote> espacoPacote = repository.findById(id);
        return espacoPacote.get();
    }

    public Espaco_Pacote findByTipoContratacao(TipoContratacao tipoContratacao){
        return repository.findByTipoContratacao( tipoContratacao );
    }

    public Espaco_Pacote findByQuantidade( Integer quantidade){
        return repository.findByQuantidade( quantidade );
    }

    public  Espaco_Pacote findByPrazo( Integer prazo ){
        return repository.findByPrazo( prazo );
    }

    public void delete( Integer id){
        repository.deleteById( id );
    }
}
