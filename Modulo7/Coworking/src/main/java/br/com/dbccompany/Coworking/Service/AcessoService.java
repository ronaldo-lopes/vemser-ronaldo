package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.Acesso;
import br.com.dbccompany.Coworking.Repository.AcessoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class AcessoService {
    @Autowired
    private AcessoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Acesso salvar(Acesso acesso) {
        return repository.save(acesso);
    }

    @Transactional(rollbackFor = Exception.class)
    public Acesso editar(Acesso acesso, Integer id) {
        acesso.setId(id);
        return repository.save(acesso);
    }

    public List<Acesso> todosAcessos() {
        return repository.findAll();
    }

    public Acesso acessoEspecifico(Integer id) {
        Optional<Acesso> acesso = repository.findById(id);
        return acesso.get();
    }

    public Acesso findByDate(Date data) {
        return repository.findByData(data);
    }

    public void delete(Integer id) {
        repository.deleteById(id);
    }
}
