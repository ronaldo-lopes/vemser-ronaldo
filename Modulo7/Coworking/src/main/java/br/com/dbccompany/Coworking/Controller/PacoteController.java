package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.Pacote;
import br.com.dbccompany.Coworking.Service.PacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacote")
public class PacoteController {
    @Autowired
    PacoteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Pacote> todosPacotes() {

        return service.todosPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pacote novoPacote(@RequestBody Pacote pacote) {

        return service.salvar(pacote);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Pacote editarPacote(@PathVariable Integer id, @RequestBody Pacote pacote) {
        return service.editar(pacote, id);
    }

    @GetMapping(value = "/valor/{id}")
    @ResponseBody
    public Pacote buscarPorValor( Double valor ){
        return service.findByValor( valor );
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( Integer id ){
        service.delete( id );
        return true;
    }
}
