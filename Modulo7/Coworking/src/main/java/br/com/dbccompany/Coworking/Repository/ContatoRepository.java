package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.Contato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface ContatoRepository extends CrudRepository<Contato, Integer> {
    Contato findByValor(Double valor);

    List<Contato> findAll();

}
