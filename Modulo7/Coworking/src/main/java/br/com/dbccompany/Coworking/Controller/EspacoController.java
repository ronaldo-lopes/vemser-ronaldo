package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.Espaco;
import br.com.dbccompany.Coworking.Service.EspacoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espaco")
public class EspacoController {
    @Autowired
    EspacoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Espaco> todosEspacos() {

        return service.todosEspacos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Espaco novoEspaco(@RequestBody Espaco espaco) {

        return service.salvar(espaco);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Espaco editarEspaco(@PathVariable Integer id, @RequestBody Espaco espaco) {
        return service.editar(espaco, id);
    }

    @GetMapping(value = "/nome/{nome}")
    @ResponseBody
    public Espaco buscarPorNomeEspaco( String nome ){
        return service.findByNome( nome );
    }

    @GetMapping(value = "/qtdPessoas/{qtdPessoas}")
    @ResponseBody
    public Espaco buscarPorQtdPessoas( Integer qtdPessoas ){
        return service.findByQtdPessoas(qtdPessoas);
    }

    @GetMapping(value = "/valor/{valor}")
    @ResponseBody
    public Espaco buscarPorValor( Double valor ){
        return service.findByValor( valor );
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete(Integer id){
        service.delete(id);
        return true;
    }
}
