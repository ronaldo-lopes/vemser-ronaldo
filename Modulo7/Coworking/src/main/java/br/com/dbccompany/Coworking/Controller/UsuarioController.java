package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.Usuario;
import br.com.dbccompany.Coworking.Service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController {
    @Autowired
    UsuarioService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Usuario> todosUsuarios() {

        return service.todosUsuarios();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Usuario novoUsuario(@RequestBody Usuario usuario) {

        return service.salvar(usuario);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Usuario editarUsuario(@PathVariable Integer id, @RequestBody Usuario usuario) {
        return service.editar(usuario, id);
    }

    @GetMapping( value = "/email/{email}")
    @ResponseBody
    public Usuario buscarPorEmail(@PathVariable String email){
        return service.findByEmail( email );
    }

    @GetMapping( value = "/login/{login}")
    @ResponseBody
    public Usuario buscarPorLogin(@PathVariable String login){
        return service.findByLogin( login );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Usuario buscarPorIdUsuario(@PathVariable Integer id){
        return service.UsuarioEspecifico( id );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id){
        service.delete( id );
        return true;
    }

}
