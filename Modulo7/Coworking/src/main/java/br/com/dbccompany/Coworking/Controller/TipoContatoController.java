package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.Tipo_Contato;
import br.com.dbccompany.Coworking.Service.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoConta")
public class TipoContatoController {
    @Autowired
    TipoContatoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Tipo_Contato> todosTiposContatos() {

        return service.todosTiposContatos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Tipo_Contato novoTipoContato(@RequestBody Tipo_Contato tipoContato) {

        return service.salvar(tipoContato);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Tipo_Contato editarTipoContato(@PathVariable Integer id, @RequestBody Tipo_Contato tipoContato) {
        return service.editar(tipoContato, id);
    }

    @GetMapping(value = "/nome/{nome}")
    @ResponseBody
    public Tipo_Contato buscarPorNomeTipoContato( String nome ){
        return service.findByNome(nome);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( Integer id ){
        service.delete( id );
        return true;
    }
}
