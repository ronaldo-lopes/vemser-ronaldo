package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.Tipo_Contato;
import br.com.dbccompany.Coworking.Repository.TipoContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContatoService {
    @Autowired
    private TipoContatoRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Tipo_Contato salvar(Tipo_Contato tipoContato) {
        return repository.save(tipoContato);
    }

    @Transactional(rollbackFor = Exception.class)
    public Tipo_Contato editar(Tipo_Contato tipoContato, Integer id) {
        tipoContato.setId(id);
        return repository.save(tipoContato);
    }

    public List<Tipo_Contato> todosTiposContatos() {
        return repository.findAll();
    }

    public Tipo_Contato TiposContatosEspecificos(Integer id) {
        Optional<Tipo_Contato> tipoContato = repository.findById(id);
        return tipoContato.get();
    }

    public Tipo_Contato findByNome(String nome){
        return repository.findByNome( nome );
    }

    public void delete( Integer id){
        repository.deleteById( id );
    }
}
