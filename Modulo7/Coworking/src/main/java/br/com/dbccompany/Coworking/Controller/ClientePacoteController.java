package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.Cliente_Pacote;
import br.com.dbccompany.Coworking.Service.ClientePacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientePacote")
public class ClientePacoteController {
    @Autowired
    ClientePacoteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Cliente_Pacote> todosClientesPacotes() {

        return service.todosClientesPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Cliente_Pacote novoClientePacote(@RequestBody Cliente_Pacote clientePacote) {

        return service.salvar(clientePacote);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Cliente_Pacote editarClientePacote(@PathVariable Integer id, @RequestBody Cliente_Pacote clientePacote) {
        return service.editar(clientePacote, id);
    }

    @GetMapping( value = "/quantidade/{quantidade}")
    @ResponseBody
    public Cliente_Pacote buscarPorQuantidadeClientePacote( Integer quantidade){
        return service.findByQuantidade( quantidade );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id){
        service.delete( id );
        return true;
    }
}
