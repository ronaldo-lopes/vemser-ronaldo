package br.com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "TIPO_CONTATO")
public class Tipo_Contato {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "TIPO_CONTATO_SEQ", sequenceName = "TIPO_CONTATO_SEQ")
    @GeneratedValue(generator = "TIPO_CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_TIPO_CONTATO", nullable = false)
    private Integer id;
    private String nome;

    @ManyToMany(mappedBy = "tipoContato")
    private List<Contato> contato;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Contato> getContato() {
        return contato;
    }

    public void setContato(List<Contato> contato) {
        this.contato = contato;
    }
}
