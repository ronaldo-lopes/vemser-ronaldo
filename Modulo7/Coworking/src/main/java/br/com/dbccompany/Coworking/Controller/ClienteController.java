package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.Cliente;
import br.com.dbccompany.Coworking.Service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/Cliente")
public class ClienteController {
    @Autowired
    ClienteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Cliente> todosClientes() {

        return service.todosClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Cliente novoCliente(@RequestBody Cliente cliente) {

        return service.salvar(cliente);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Cliente editarCliente(@PathVariable Integer id, @RequestBody Cliente cliente) {
        return service.editar(cliente, id);
    }

    @GetMapping( value = "/nome/{nome}")
    @ResponseBody
    public Cliente buscarPorNomeCliente( String nome ){
        return service.findByNome( nome );
    }

    @GetMapping( value = "/cpf/{cpf}")
    @ResponseBody
    public Cliente buscarPorCpf( String cpf ){
        return service.findByCpf( cpf );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id){
        service.delete( id );
        return true;
    }
}
