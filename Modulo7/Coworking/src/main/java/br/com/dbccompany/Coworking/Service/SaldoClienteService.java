package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.SaldoCliente;
import br.com.dbccompany.Coworking.Entity.Saldo_ClienteId;
import br.com.dbccompany.Coworking.Entity.TipoContratacao;
import br.com.dbccompany.Coworking.Repository.SaldoClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SaldoClienteService {
    @Autowired
    SaldoClienteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente salvar(SaldoCliente saldoCliente) {
        return repository.save(saldoCliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public SaldoCliente editar(SaldoCliente saldoCliente, Saldo_ClienteId id) {
        saldoCliente.setId(id);
        return repository.save(saldoCliente);
    }

    public List<SaldoCliente> todosSaldosClientes() {
        return repository.findAll();
    }

    public SaldoCliente saldoClienteEspecifico(Saldo_ClienteId id) {
        Optional<SaldoCliente> saldoCliente = repository.findById(id);
        return saldoCliente.get();
    }

    public SaldoCliente findByTipoContratacao(TipoContratacao tipoContratacao){
        return repository.findByTipoContratacao( tipoContratacao );
    }

    public SaldoCliente findByQuantidade(Integer quantidade){
        return repository.findByQuantidade( quantidade );
    }

    public SaldoCliente findByData(Date vencimento){
        return repository.findByData( vencimento );
    }

    public void delete( Saldo_ClienteId id){
        repository.deleteById( id );
    }
}
