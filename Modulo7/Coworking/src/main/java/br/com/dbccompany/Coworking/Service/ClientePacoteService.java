package br.com.dbccompany.Coworking.Service;

import br.com.dbccompany.Coworking.Entity.Cliente_Pacote;
import br.com.dbccompany.Coworking.Repository.ClientePacoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ClientePacoteService {
    @Autowired
    private ClientePacoteRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Cliente_Pacote salvar(Cliente_Pacote cliente_pacote) {
        return repository.save(cliente_pacote);
    }

    @Transactional(rollbackFor = Exception.class)
    public Cliente_Pacote editar(Cliente_Pacote clientePacote, Integer id) {
        clientePacote.setId(id);
        return repository.save(clientePacote);
    }

    public List<Cliente_Pacote> todosClientesPacotes() {
        return repository.findAll();
    }

    public Cliente_Pacote clientePacoteEspecifico(Integer id) {
        Optional<Cliente_Pacote> clientePacote = repository.findById(id);
        return clientePacote.get();
    }

    public Cliente_Pacote findByQuantidade(Integer quantidade) {
        return repository.findByQuantidade(quantidade);
    }

    public void delete(Integer id) {
        repository.deleteById(id);
    }

}
