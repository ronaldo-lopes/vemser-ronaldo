package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.Espaco_Pacote;
import br.com.dbccompany.Coworking.Entity.TipoContratacao;
import br.com.dbccompany.Coworking.Service.EspacoPacoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espaco")
public class EspacoPacoteController {
    @Autowired
    EspacoPacoteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Espaco_Pacote> todosEspacosPacotes() {

        return service.todosEspacosPacotes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Espaco_Pacote novoEspacoPacote(@RequestBody Espaco_Pacote espacoPacote) {

        return service.salvar(espacoPacote);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Espaco_Pacote editarEspacoPacote(@PathVariable Integer id, @RequestBody Espaco_Pacote espacoPacote) {
        return service.editar(espacoPacote, id);
    }

    @GetMapping(value = "/tipoContratacao/{tipoContratacao}")
    @ResponseBody
    public Espaco_Pacote buscarPorTipoContratacao(TipoContratacao tipoContratacao){
        return service.findByTipoContratacao(tipoContratacao);
    }

    @GetMapping(value = "/quantidade/{quantidade}")
    @ResponseBody
    public Espaco_Pacote buscarPorQuantidadeEspacoPacote( Integer quantidade){
        return service.findByQuantidade(quantidade);
    }

    @GetMapping(value = "/prazo/{prazo}")
    @ResponseBody
    public Espaco_Pacote buscarPorValor( Integer prazo){
        return service.findByPrazo( prazo );
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete(Integer id){
        service.delete(id);
        return true;
    }

}
