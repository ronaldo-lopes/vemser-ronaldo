package br.com.dbccompany.Coworking.Service;


import br.com.dbccompany.Coworking.Entity.Usuario;
import br.com.dbccompany.Coworking.Repository.UsuarioRepository;
import br.com.dbccompany.Coworking.Security.Criptografia;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository repository;

    @Transactional(rollbackFor = Exception.class)
    public Usuario salvar(Usuario usuario) {
        String senha = usuario.getSenha();
        if( senha.length() >= 6 && StringUtils.isAlphanumeric(senha) ){
            usuario.setSenha(Criptografia.md5(senha));
        }else{
            throw new RuntimeException(("Digite sua senha com 6 digitos e alfanumericos"));
        }
        return repository.save(usuario);
    }

    @Transactional(rollbackFor = Exception.class)
    public Usuario editar(Usuario usuario, Integer id) {
        usuario.setId(id);
        return repository.save(usuario);
    }

    public List<Usuario> todosUsuarios() {
        return repository.findAll();
    }

    public Usuario UsuarioEspecifico(Integer id) {
        Optional<Usuario> usuario = repository.findById(id);
        return usuario.get();
    }

    public Usuario findByNome( String nome ){
        return repository.findByNome( nome );
    }

    public Usuario findByEmail( String email ){
        return repository.findByEmail( email );
    }

    public Usuario findByLogin( String login ){
        return repository.findByLogin( login );
    }

    public void delete( Integer id){
        repository.deleteById( id );
    }
}
