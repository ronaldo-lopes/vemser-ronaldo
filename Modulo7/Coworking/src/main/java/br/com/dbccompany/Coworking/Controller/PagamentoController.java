package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.Pagamento;
import br.com.dbccompany.Coworking.Entity.TipoPagamento;
import br.com.dbccompany.Coworking.Service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamento")
public class PagamentoController {
    @Autowired
    PagamentoService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<Pagamento> todosPagamentos() {

        return service.todosPagamentos();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public Pagamento novoPagamento(@RequestBody Pagamento pagamento) {

        return service.salvar(pagamento);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public Pagamento editarPagamento(@PathVariable Integer id, @RequestBody Pagamento pagamento) {
        return service.editar(pagamento, id);
    }

    @GetMapping(value = "/tipoPagamento/{tipoPagamento}")
    @ResponseBody
    public Pagamento buscarPorTipoPagamento(TipoPagamento tipoPagamento){
        return service.findByTipoPagamento(tipoPagamento);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( Integer id){
        service.delete( id );
        return true;
    }
}
