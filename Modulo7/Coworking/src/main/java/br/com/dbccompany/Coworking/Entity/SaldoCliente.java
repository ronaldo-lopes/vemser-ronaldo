package br.com.dbccompany.Coworking.Entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "SALDO_CLIENTE")
public class SaldoCliente {

    @EmbeddedId
    private Saldo_ClienteId id;

    @ManyToOne
    @MapsId("cliente_id")
    @JoinColumn(name = "cliente_id")
    private Cliente cliente;

    @ManyToOne
    @MapsId("espaco_id")
    @JoinColumn(name = "espaco_id")
    private Espaco espaco;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    private Integer quantidade;

    @DateTimeFormat( pattern = "dd/MM/yyyy")
    private Date vencimento;

    public Saldo_ClienteId getId() {
        return id;
    }

    public void setId(Saldo_ClienteId id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }
}
