package br.com.dbccompany.Coworking.Entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ACESSOS")
public class Acesso {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "ACESSO_SEQ", sequenceName = "ACESSO_SEQ")
    @GeneratedValue(generator = "ACESSO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_ACESSO", nullable = false)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("cliente_id")
    @JoinColumn(name = "clientes_saldo_clientes_id")
    private Cliente cliente;

    @ManyToOne(cascade = CascadeType.ALL)
    @MapsId("espacos_id")
    @JoinColumn(name = "espacos_saldo_espaco_id")
    private Espaco espaco;

    private Boolean isEntrada;

    private Boolean isExcecao;

    @Temporal(TemporalType.DATE)
    private Date data;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Espaco getEspaco() {
        return espaco;
    }

    public void setEspaco(Espaco espaco) {
        this.espaco = espaco;
    }

    public Boolean getEntrada() {
        return isEntrada;
    }

    public void setEntrada(Boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Boolean getExcecao() {
        return isExcecao;
    }

    public void setExcecao(Boolean excecao) {
        isExcecao = excecao;
    }
}
