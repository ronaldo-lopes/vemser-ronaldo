package br.com.dbccompany.Coworking.Controller;

import br.com.dbccompany.Coworking.Entity.SaldoCliente;
import br.com.dbccompany.Coworking.Entity.Saldo_ClienteId;
import br.com.dbccompany.Coworking.Entity.TipoContratacao;
import br.com.dbccompany.Coworking.Service.SaldoClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/api/saldoCliente")
public class SaldoClienteController {
    @Autowired
    SaldoClienteService service;

    @GetMapping(value = "/todos")
    @ResponseBody
    public List<SaldoCliente> todosSaldosClientes() {

        return service.todosSaldosClientes();
    }

    @PostMapping(value = "/novo")
    @ResponseBody
    public SaldoCliente novoSaldoCliente(@RequestBody SaldoCliente saldoCliente) {

        return service.salvar(saldoCliente);
    }

    @PutMapping(value = "/editar/{id}")
    @ResponseBody
    public SaldoCliente editarSaldoCliente(@PathVariable Saldo_ClienteId id, @RequestBody SaldoCliente saldoCliente) {
        return service.editar(saldoCliente, id);
    }

    @GetMapping(value = "/tipoContratacao/{tipoContratacao}")
    @ResponseBody
    public SaldoCliente buscarPorTipoContratacaoSaldoCliente(TipoContratacao tipoContratacao){
        return service.findByTipoContratacao(tipoContratacao);
    }

    @GetMapping(value = "/data/{data}")
    @ResponseBody
    public SaldoCliente buscarPorDataSaldoCliente( Date data ){
        return service.findByData(data);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete(Saldo_ClienteId id){
        service.delete(id);
        return true;
    }
}
