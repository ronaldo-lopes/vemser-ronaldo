package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.SaldoCliente;
import br.com.dbccompany.Coworking.Entity.Saldo_ClienteId;
import br.com.dbccompany.Coworking.Entity.TipoContratacao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoCliente, Saldo_ClienteId> {
    SaldoCliente findByTipoContratacao(TipoContratacao tipoContratacao);

    SaldoCliente findByQuantidade(Integer quantidade);

    SaldoCliente findByData(Date vencimento);

    List<SaldoCliente> findAll();
}
