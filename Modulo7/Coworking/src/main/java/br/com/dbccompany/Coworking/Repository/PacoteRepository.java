package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.Pacote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PacoteRepository extends CrudRepository<Pacote, Integer> {
    Pacote findByValor(Double valor);

    List<Pacote> findAll();
}
