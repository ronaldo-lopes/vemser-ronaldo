package br.com.dbccompany.Coworking.Entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "CLIENTES")
public class Cliente {
    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CLIENTE", nullable = false)
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(nullable = false, unique = true, length = 14)
    private String cpf;

    @Column( nullable = false )
    @DateTimeFormat( pattern = "dd/MM/yyyy")
    private Date dataNascimento;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "contato_id", nullable = false)
    private Contato contato;

    @OneToMany(mappedBy = "cliente")
    List<SaldoCliente> saldos;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }
}
