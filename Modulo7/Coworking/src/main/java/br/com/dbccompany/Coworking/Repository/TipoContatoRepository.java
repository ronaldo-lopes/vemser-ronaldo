package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.Tipo_Contato;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TipoContatoRepository extends CrudRepository<Tipo_Contato, Integer> {
    Tipo_Contato findByNome(String nome);

    List<Tipo_Contato> findAll();
}
