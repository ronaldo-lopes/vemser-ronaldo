package br.com.dbccompany.Coworking.Repository;

import br.com.dbccompany.Coworking.Entity.Cliente_Pacote;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientePacoteRepository extends CrudRepository<Cliente_Pacote, Integer> {
    Cliente_Pacote findByQuantidade(Integer quantidade);

    List<Cliente_Pacote> findAll();

}
