package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Estado;
import br.com.dbccompany.bancodigital.Service.EstadosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/estado")
public class EstadoController {
    @Autowired
    EstadosService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<Estado> todosEstadps(){
        return service.todosEstados();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public Estado novoEstado(@RequestBody Estado estado){
        return  service.salvar(estado);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public Estado editarEstado(@PathVariable Integer id, @RequestBody Estado estado){
        return  service.editar(estado, id);
    }

    @GetMapping( value = "/{nome}" )
    @ResponseBody
    public Estado buscarPorNomeEstado(@PathVariable String nome ){
        return service.findByNome( nome );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Estado buscarPorIdEstado(@PathVariable Integer id ){
        return service.estadoEspecifico( id );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }
}
