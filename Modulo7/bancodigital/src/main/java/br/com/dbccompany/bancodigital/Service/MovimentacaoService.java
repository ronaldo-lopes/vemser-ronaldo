package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Movimentacao;
import br.com.dbccompany.bancodigital.Entity.Tipo_Movimentacao;
import br.com.dbccompany.bancodigital.Repository.MovimentacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class MovimentacaoService {
    @Autowired
    private MovimentacaoRepository repository;

    @Transactional
    public Movimentacao salvar(Movimentacao movimentacao){
        return repository.save(movimentacao);
    }

    @Transactional
    public Movimentacao editar(Movimentacao movimentacao, Integer id){
        movimentacao.setId(id);
        return repository.save(movimentacao);
    }

    public List<Movimentacao> todasMovimentacoes(){
        return repository.findAll();
    }

    public Movimentacao movimentacaoEspecifica(Integer id){
        Optional<Movimentacao> movimentacao = repository.findById(id);
        return movimentacao.get();
    }

    public Movimentacao findByTipoMovimentacao( Tipo_Movimentacao tipoMovimentacao ){
        Optional<Movimentacao> movimentacao = repository.findByTipoMovimentacao( tipoMovimentacao );
        return movimentacao.get();
    }

    public Movimentacao findByValor( Double valor ){
        return repository.findByValor( valor );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }



}
