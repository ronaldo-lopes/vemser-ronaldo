package br.com.dbccompany.bancodigital.Entity;

import org.springframework.scheduling.support.SimpleTriggerContext;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CLIENTES")
public class Cliente {
    @Id
    @SequenceGenerator( allocationSize = 1, name = "CLIENTES_SEQ", sequenceName = "CLIENTES_SEQ")
    @GeneratedValue(generator = "CLIENTES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CLIENTES", nullable = false)
    private Integer id;

    private  String cpf;

    private  String nome;

    public Cliente(){}

    public Cliente(Integer id, String cpf, String nome){
        this.id = id;
        this.cpf = cpf;
        this.nome = nome;
    }

    @ManyToMany
    @JoinTable( name = "CLIENTE_CONTA",
                joinColumns = { @JoinColumn( name = "CLIENTE_ID")},
                 inverseJoinColumns = { @JoinColumn( name = "CONTA_ID")})
    private List<Conta> contas = new ArrayList<>();

    @ManyToMany
    @JoinTable( name = "CIDADE_CLIENTE",
            joinColumns = { @JoinColumn( name = "CLIENTE_ID")},
            inverseJoinColumns = { @JoinColumn( name = "CIDADE_ID")})
    private List<Cidade> cidades = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf (String cpf) {
        this.cpf = cpf;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Conta> getContas() {
        return contas;
    }

    public void setContas(List<Conta> contas) {
        this.contas = contas;
    }

    public List<Cidade> getCidades() {
        return cidades;
    }

    public void setCidades(List<Cidade> cidades) {
        this.cidades = cidades;
    }
}
