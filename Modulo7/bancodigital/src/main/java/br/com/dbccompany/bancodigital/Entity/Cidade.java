package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CIDADES")
public class Cidade {
    @Id
    @SequenceGenerator( allocationSize = 1, name = "CIDADES_SEQ", sequenceName = "CIDADES_SEQ")
    @GeneratedValue(generator = "CIDADES_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CIDADES", nullable = false)
    private Integer id;

    private String nome;

    public Cidade(){}

    public Cidade(Integer id, String nome){
        this.id = id;
        this.nome = nome;
    }

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "ESTADO_ID")
    private Estado estado;

    @OneToMany( mappedBy = "cidade")
    private List<Agencia> agencia;

    @ManyToMany
    @JoinTable( name = "CIDADE_CLIENTE",
                 joinColumns = { @JoinColumn( name = "CIDADE_ID")},
                 inverseJoinColumns = { @JoinColumn( name = "CLIENTE_ID")})
    private List<Cliente> cliente = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public List<Cliente> getClientes() {
        return cliente;
    }

    public void setClientes(List<Cliente> cliente) {
        this.cliente = cliente;
    }

    public List<Agencia> getAgencias() {
        return agencia;
    }

    public void setAgencias(List<Agencia> agencias) {
        this.agencia = agencia;
    }
}
