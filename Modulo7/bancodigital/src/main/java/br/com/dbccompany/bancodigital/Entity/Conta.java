package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="CONTAS")
public class Conta {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "CONTA_SEQ", sequenceName = "CONTA_SEQ")
    @GeneratedValue(generator = "CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_CONTA", nullable = false)
    private Integer id;
    private Integer numero;
    private Double saldo;

    public Conta(){}

    public Conta(Integer id, Integer numero, Double saldo){
        this.id = id;
        this.numero = numero;
        this.saldo = saldo;
    }

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "TIPO_CONTA_ID")
    private Tipo_Conta tipoConta;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "AGENCIA_ID")
    private Agencia agencia;

    @ManyToOne( cascade = CascadeType.ALL)
    @JoinColumn( name = "MOVIMENTACAO_ID")
    private Movimentacao movimentacao;

    @ManyToMany
    @JoinTable(
            name = "CLIENTE_CONTA",
            joinColumns = { @JoinColumn( name = "CONTA_ID")},
            inverseJoinColumns = { @JoinColumn( name = "CLIENTE_ID")})
    private List<Cliente> clientes = new ArrayList<>();


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Tipo_Conta getTipo_conta() {
        return tipoConta;
    }

    public void setTipo_conta(Tipo_Conta tipo_conta) {
        this.tipoConta = tipo_conta;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public Movimentacao getMovimentacao() {
        return movimentacao;
    }

    public void setMovimentacao(Movimentacao movimentacao) {
        this.movimentacao = movimentacao;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
}
