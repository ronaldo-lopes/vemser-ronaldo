package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.Movimentacao;
import br.com.dbccompany.bancodigital.Entity.Tipo_Movimentacao;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface MovimentacaoRepository extends CrudRepository<Movimentacao, Integer> {
    Movimentacao findByValor( Double valor );
    Optional<Movimentacao> findByTipoMovimentacao(Tipo_Movimentacao tipoMovimentacao);
    List<Movimentacao> findAll();
}
