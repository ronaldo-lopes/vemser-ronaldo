package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "TIPO_CONTA")
public class Tipo_Conta {

    @Id
    @SequenceGenerator( allocationSize = 1, name = "TIPO_CONTA_SEQ", sequenceName = "TIPO_CONTA_SEQ")
    @GeneratedValue(generator = "TIPO_CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_TIPO_CONTA", nullable = false)
    private Integer id;

    private Descricao_Contas descricao;

    public Tipo_Conta(){}

    public Tipo_Conta(Integer id, Descricao_Contas descricao){
        this.id = id;
        this.descricao = descricao;
    }

   @OneToMany( mappedBy = "tipoConta")
    private List<Conta> conta;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Conta> getContas() {
        return conta;
    }

    public void setContas(List<Conta> contas) {
        this.conta = contas;
    }

    public Descricao_Contas getDescricao() {
        return descricao;
    }

    public void setDescricao(Descricao_Contas descricao) {
        this.descricao = descricao;
    }
}
