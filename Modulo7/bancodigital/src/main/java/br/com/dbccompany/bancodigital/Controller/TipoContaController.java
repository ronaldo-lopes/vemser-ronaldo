package br.com.dbccompany.bancodigital.Controller;

import br.com.dbccompany.bancodigital.Entity.Descricao_Contas;
import br.com.dbccompany.bancodigital.Entity.Tipo_Conta;
import br.com.dbccompany.bancodigital.Service.TipoContaService;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping( "/api/TipoConta")
public class TipoContaController {

    @Autowired
    TipoContaService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<Tipo_Conta> todosTiposContas(){
        return service.todosTiposContas();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public  Tipo_Conta  novoTipoConta(@RequestBody Tipo_Conta tipo){
        return service.salvar(tipo);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public Tipo_Conta editarTipoBanco(@PathVariable Integer id, @RequestBody Tipo_Conta tipo){
        return service.editar(tipo, id);
    }

    @GetMapping( value = "/{descricao}" )
    @ResponseBody
    public Tipo_Conta buscarPorDescricaoTiposConta(@PathVariable Descricao_Contas descricao ){
        return service.findByDescricao( descricao );
    }

    @GetMapping( value = "/{id}" )
    @ResponseBody
    public Tipo_Conta buscarPorIdTiposConta(@PathVariable Integer id ){
        return service.TipoContaEspecifica( id );
    }

    @RequestMapping( value = "/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public Boolean delete( @PathVariable Integer id ) {
        service.delete( id );
        return true;
    }
}
