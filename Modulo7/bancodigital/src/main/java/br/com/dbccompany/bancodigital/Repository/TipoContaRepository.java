package br.com.dbccompany.bancodigital.Repository;

import br.com.dbccompany.bancodigital.Entity.Descricao_Contas;
import br.com.dbccompany.bancodigital.Entity.Tipo_Conta;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TipoContaRepository extends CrudRepository<Tipo_Conta, Integer> {
    Tipo_Conta findByDescricao(Descricao_Contas descricao);
    List<Tipo_Conta> findAll();
}
