package br.com.dbccompany.bancodigital.Service;

import br.com.dbccompany.bancodigital.Entity.Descricao_Contas;
import br.com.dbccompany.bancodigital.Entity.Tipo_Conta;
import br.com.dbccompany.bancodigital.Repository.TipoContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TipoContaService {
    @Autowired
    private TipoContaRepository repository;

    @Transactional
    public Tipo_Conta salvar(Tipo_Conta tipo){
        return  repository.save(tipo);
    }

    @Transactional
    public  Tipo_Conta editar(Tipo_Conta tipo, Integer id){
        tipo.setId(id);
        return repository.save(tipo);
    }

    public List<Tipo_Conta> todosTiposContas(){
        return repository.findAll();
    }

    public Tipo_Conta TipoContaEspecifica( Integer id ){
        Optional<Tipo_Conta> tipo = repository.findById(id);
        return tipo.get();
    }

    public Tipo_Conta findByDescricao( Descricao_Contas descricao ){
        return repository.findByDescricao( descricao );
    }

    public void delete( Integer id ){
        repository.deleteById( id );
    }
}
