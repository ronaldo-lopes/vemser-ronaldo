package br.com.dbccompany.bancodigital.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "MOVIMENTACOES")
public class Movimentacao {
    @Id
    @SequenceGenerator( allocationSize = 1, name = "MOVIMENTACAO_SEQ", sequenceName = "MOVIMENTACAO_SEQ")
    @GeneratedValue(generator = "MOVIMENTACAO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(name = "ID_MOVIMENTACAO", nullable = false)
    private Integer id;

    private Double valor;

    private Tipo_Movimentacao tipoMovimentacao;

    public Movimentacao(){}

    public Movimentacao(Integer id, Tipo_Movimentacao tipoMovimentacao, Double valor){
        this.id = id;
        this.tipoMovimentacao = tipoMovimentacao;
        this.valor = valor;
    }

    @OneToMany( mappedBy = "movimentacao")
    private List<Conta> contas;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        valor = valor;
    }

    public List<Conta> getContas() {
        return contas;
    }

    public void setContas(List<Conta> contas) {
        this.contas = contas;
    }

    public Tipo_Movimentacao getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    public void setTipoMovimentacao(Tipo_Movimentacao tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }
}
